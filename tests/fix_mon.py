import json,os

def fix_MON_column(inp,outp):
    with open(inp,'r') as in_f:
        with open(outp,'a') as out_f:
            for i,l in enumerate(in_f):
                if i>6:
                    print(l[:34],l[34:-1],file=out_f)
                else:
                    print(l[:-1],file=out_f)
            out_f.flush()

file = '/Users/au261214/projects/swat_app/swat_demo/seest/output.hru'

if __name__=='__main__':
    import sys
    print(sys.argv)
    fix_MON_column(file,file+'.new')
