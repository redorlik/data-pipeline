import tempfile
from ..read_swat_output import (read_header,check_header,fix_MON_column,
                del_onCp,find_ends,get_data)

seest_file = '/Users/au261214/projects/swat_app/swat_demo/seest/output.hru.new'
head = '/LmmL TOT Pkg'
head2 = 'LULC  HRU       GIS  SUB  MGT  MON   AREAkm2  PRECIPmm SNOFALLmm SNOMELTmm     IRRmm     PETmm      ETmm SW_INITmm  SW_ENDmm    PERCmm GW_RCHGmm DA_RCHGmm   REVAPmm  SA_IRRmm  DA_IRRmm   SA_STmm   DA_STmmSURQ_GENmmSURQ_CNTmm   TLOSSmm LATQGENmm    GW_Qmm    WYLDmm   DAILYCN TMP_AVdgC TMP_MXdgC TMP_MNdgCSOL_TMPdgCSOLARMJ/m2  SYLDt/ha  USLEt/haN_APPkg/haP_APPkg/haNAUTOkg/haPAUTOkg/ha NGRZkg/ha PGRZkg/haNCFRTkg/haPCFRTkg/haNRAINkg/ha NFIXkg/ha F-MNkg/ha A-MNkg/ha A-SNkg/ha F-MPkg/haAO-LPkg/ha L-APkg/ha A-SPkg/ha DNITkg/ha  NUPkg/ha  PUPkg/ha ORGNkg/ha ORGPkg/ha SEDPkg/haNSURQkg/haNLATQkg/ha NO3Lkg/haNO3GWkg/ha SOLPkg/ha P_GWkg/ha    W_STRS  TMP_STRS    N_STRS    P_STRS  BIOMt/ha       LAI   YLDt/ha  BACTPct   BACTLPct WTAB CLIm WTAB SOLm     SNOmm CMUPkg/haCMTOTkg/ha   QTILEmm TNO3kg/ha LNO3kg/ha  GW_Q_Dmm LATQCNTmm TVAPkg/ha'
dir = [('/Users/au261214/pCloud Drive/Shared/WaterWebTools/01_KAIPING/SWAT_setup/example/dashahe/Scenarios/Default/TxtInOut/','kaiping'),
   ('/Users/au261214/projects/swat_app/swat_demo/seest','seest')]

def notest_read_rch():
    assert read_header(seest_file,9) == ''

def notest_header_convert():
    assert '/L' not in check_header(head)

def notest_check_header():
    res=check_header(head2)
    print(res)
    assert 'L' in res

def test_simpletmp_divide():
    test = 'TMP_MNdgCSOL_TMPdgCSOLARMJ/m2'
    assert del_onCp(test) ==['TMP_MNdgC','SOL_TMPdgC','SOLARMJ/m2']

def test_divide_header():
    head = check_header(head2)
    res = [del_onCp(x) for x in head]
    rm = []
    for x in res:
        rm.extend(x)
    print(rm)
    assert len(rm) == 86

def increasing(l):
    increase = (y-x for x,y in zip(l[:-1],l[1:]))
    return (increase)

def test_find_positions():
    head = check_header(head2)
    res = [del_onCp(x) for x in head]#'DA_STmmSURQ_GENmmSURQ_CNTmm')
    rm = []
    for x in res:
        rm.extend(x)
    end = find_ends(' '.join(head),rm)
    inc = list(increasing(end))
    assert all(increasing(end))

def test_read_data():
    read_data(seest_file,9)

def read_data(seestfile,rowskip):
    dat = get_data(seestfile,rowskip)
    #print(dat[10])
    assert dat['MON'].dtype == 'int'
    assert not dat.isnull().values.any()
    for c in dat.columns:
        if c not in ['AREAkm2','LULC','HRU','SUB','YEAR','MO','RCH']:
            assert dat[c].dtype == float

def test_all_files():
    for p,o in dir:
        for ext in ['hru','sub','rch']:
            if ext in ['hru','sub']:
                if not(ext == 'sub' and o=='seest'):
                    testfile=f'testfile.{ext}'
                    fix_MON_column(f'{p}/output.{ext}',testfile)
                else:
                    testfile = f'{p}/output.{ext}'
                read_data(testfile,9)
