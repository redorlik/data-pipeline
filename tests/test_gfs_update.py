from ..GFS_data2sql import update_gfs_table
import psycopg2 as pg

def test_update():
    con = pg.connect("postgres://postgres@localhost:5432/CDS")
    con.autocommit = True
    with con:
        cur = con.cursor()
        cur.execute("update gfs set t2m = 0;")
    with con:
        cur = con.cursor()
        cur.execute("select t2m from gfs;")
        a = cur.fetchall()
        assert sum([x[0] for x in a]) == 0
    con.close()
    update_gfs_table(56.4,9.4)
    con = pg.connect("postgres://postgres@localhost:5432/CDS")
    con.autocommit = True
    with con:
        cur = con.cursor()
        cur.execute("select t2m from gfs;")
        a = cur.fetchall()
        assert sum([x[0] for x in a]) > 0
