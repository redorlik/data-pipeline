from ..Climate_data import get_fullday_from_db
from datetime import date

host = '10.42.5.110'
def test_fullday_from_db():
    res = get_fullday_from_db(host)
    assert res[0][0] == 'kaiping'
    assert type(res[0][1]) == date
