
from .Climate_data import ( get_weather_for_location,
                            PG)
import datetime
data_dir='/Users/au261214/projects/Swat_app/wwt-era5-api/data'
start = (2020,12,1)
end = (2020,12,31)
dt_start = datetime.date(*start)
dt_end = datetime.date(*end)
days = (dt_end-dt_start).days +1

from time import perf_counter

def timing(n=1):
    def deco(f):
        def wrap(*args,**kwargs):
            timing = []
            for i in range(n):
                start = perf_counter()
                res = f(*args,**kwargs)
                timing.append(perf_counter()-start)
            mean = sum(timing)/len(timing)
            std = sum([(x-mean)**2 for x in timing])/len(timing)
            print()
            print()
            print('**************************************************************************************')
            print()
            print(f'Timing for {f.__name__}, {n} runs - mean:{mean}, std : {std}')
            print()
            print('**************************************************************************************')

            return res
        wrap.__name__ = f.__name__
        return wrap
    return deco

@timing(5)
def test_single():
    active_places = [('almind',56.25,9.50,dt_start,dt_end)]
    df = get_weather_for_location(active_places,start,end,data_dir=data_dir,agg='hour')
    assert df.get('almind').shape == (days*24,10) # lat,lon are now part of index
@timing(5)
def test_single_day_():
    active_places = [('almind',56.25,9.50,dt_start,dt_end)]
    df = get_weather_for_location(active_places,start,end,data_dir=data_dir)
    assert df.get('almind').shape == (days*24,10) # lat,lon are now part of index
@timing(5)
def test_single_dayold():
    active_places = [('almind',56.25,9.50,dt_start,dt_end)]
    df = get_weather_for_location(active_places,start,end,data_dir=data_dir,read=False)
    assert df.get('almind').shape == (days*24,12) # lat,lon are now part of index

@timing(5)
def test_daily():
    active_places = [('almind',56.15,9.56,dt_start,dt_end)]
    df = get_weather_for_location(active_places,start,end,
        data_dir=data_dir)
    assert df.get('almind').shape == (days*24,10) # lat,lon are now part of index

@timing(5)
def test_multi_daily():
    active_places = [('almind',56.5,9.5,dt_start,dt_end),('almind',56.,9.5,dt_start,dt_end),
    ('almind',56.25,9.5,dt_start,dt_end),('almind',56.75,9.5,dt_start,dt_end),
    ('almind',56.5,9.25,dt_start,dt_end),('almind',56.,9.5,dt_start,dt_end),
    ('almind',56.25,9.25,dt_start,dt_end),('almind',56.75,9.25,dt_start,dt_end)]
    df = get_weather_for_location(active_places,start,end,
        data_dir=data_dir)
    assert df.get('almind').shape == (len(active_places)*days*24,10)

@timing(5)
def test_multi_hourly():
    active_places = [('almind',56.5,9.5,dt_start,dt_end),('almind',56.,9.5,dt_start,dt_end),
    ('almind',56.25,9.5,dt_start,dt_end),('almind',56.75,9.5,dt_start,dt_end),
    ('almind',56.5,9.25,dt_start,dt_end),('almind',56.,9.5,dt_start,dt_end),
    ('almind',56.25,9.25,dt_start,dt_end),('almind',56.75,9.25,dt_start,dt_end)]
    df = get_weather_for_location(active_places,start,end,
        data_dir=data_dir,agg='hour')
    assert df.get('almind').shape == (len(active_places)*days*24,10)

def test_db():
    pg = PG(port=5432)
    day = pg.get_fullday_from_db('almind')
    print (day)

if __name__ == '__main__':
    test_single()
    test_daily()