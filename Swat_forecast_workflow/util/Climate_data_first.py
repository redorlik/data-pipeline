# -*- coding: utf-8 -*-
# Program to get data from netCDF file into SQL db
# saves the latest date to have 24 hour coverage location table

import datetime
import pandas as pd
from collections import defaultdict
import os
import xarray
import time
import numpy as np
import psycopg2 as pg
from Climate_data import (insert_location,update_location,get_weather_for_location,
    get_active_places,get_fullday_from_db,fullday)
from sqlalchemy import create_engine

data_dir='F:\\Data\\ERA5\\CDS\\global\\'
places = {'kaiping': {'lat':22.523669,'lng': 112.422059},
         'seest':{'lat':55.490070, 'lng':9.403019},
         'bosomtwe':{'lat':6.5, 'lng': 360-1.41},
         "ravn":{"lat" : 56.10, "lng" : 9.85},
         "arreskov" :{"lat" : 55.16, "lng" : 10.30  },
         "skanderborg" :{"lat" : 56.02, "lng" : 9.93},
         "bryrup":{"lat" : 56.02, "lng" : 9.52},
         "hinge":{"lat" : 56.255, "lng" : 9.50},
         "soeholm":{"lat" : 55.273, "lng" : 10.13},
         "furesoe":{"lat" : 55.80, "lng" : 12.41},
         "almind":{"lat" : 56.15, "lng" : 9.54 },
         "ormstrup":{"lat" : 56.33, "lng" : 9.64},
         "san_javier":{"lat": 37.791694, "lng": -0.819578},
         "san_javier_lagune":{"lat" : 37.723106, "lng": -0.784761},
         }
pghost = "postgresql://ale@localhost:5432/CDS"
eng = create_engine(pghost)
host = 'localhost'
 
def get_active_firsttime(host):
    query = f"select name,lat,lon,id from location where last_era5_fullday is null and active;"
    conn = pg.connect(dbname='CDS',host=host,user='ale')
    cursor = conn.cursor()
    cursor.execute(query)
    return cursor.fetchall()

if __name__ == '__main__':
    import sys
    
    name_fullday = get_fullday_from_db('kaiping',host)
    print("old fullday",name_fullday)
    end = fullday(name_fullday)
    print("sys.argv",sys.argv,end)

    if len(sys.argv)==4:
        y,m,d = [int(x) for x in sys.argv[1:4]]
        start = datetime.date(y,m,d)
    else:
        start = name_fullday
        start = datetime.date(start.year,start.month,start.day)
        start += datetime.timedelta(1)
    print("start date",start,type(start))


    active_places = [x for x in get_active_firsttime(host)]
    print(active_places)
    begin = time.perf_counter()
    if start<=end:
        df = get_weather_for_location(active_places,start,end)

        for p,_,_,_ in active_places:
            df[p].to_csv(f'{p}.dat', header=True, sep='\t', float_format='%.2f')
            finish = time.perf_counter()
            print(f"read time {finish-begin}")
            df[p].to_sql(p,eng,if_exists='replace')
            begin = time.perf_counter()
            print(f"sql time {begin-finish}")
        update_location(host,end)
