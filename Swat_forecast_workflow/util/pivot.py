from collections import defaultdict
from time import perf_counter

#import psycopg2 as pg
from datetime import datetime, timedelta, timezone
import numpy as np
import os
import xarray as xr
from attr import define

def join(x):
    return ','.join([str(y) for y in x])

def get_rows(df):
    arr = [df['time'].values]
    for i in df:
        arr.append(df[i].values)
    return zip(*arr)

class MyTimer:
    timing = defaultdict(list)

    def __init__(self,name,save=False,print=False):
        self.save = save
        self.name = name
        self.print = print

    def __enter__(self):
        self.start = perf_counter()
        if self.print: print(f'Starting {self.name}')
        return self

    def __exit__(self,exc,err,tb):
        tid = perf_counter()-self.start
        if self.print: print(f'ending {self.name}, time: {tid}')
        if self.save: self.timing[self.name].append(tid)

@define
class Pivot:

    timing = defaultdict(list)
    start : datetime
    end : datetime
    days : int
    points : int
    destination : str
    origin : str
    mask : xr.Dataset
    
    def make_mask_dict(self):
        from pickle import load
        try:
            with open('mask.pck','rb') as f:
                self.mask_dict = load(f)
        except FileNotFoundError as e:
            self.mask_dict = defaultdict(list)
            for lon in self.mask.longitude.values:
                for lat in self.mask.latitude.values:
                    try:
                        mm = self.mask.sel(latitude=lat,longitude=lon)
                    except KeyError:
                        #print('Latitude=',lat,'Longitude=',lon)
                        continue
                    if not np.isnan(mm.cl):
                        self.mask_dict[lon].append(lat)

        for lon,lats in self.mask_dict.items():
            x = 0
            while  x<len(lats)-self.points:
                folder = os.path.join(self.destination,f'{lon}',f'{lats[x]}')

                os.makedirs(folder,mode=0o777,exist_ok=True)
                x += self.points
            if x<len(lats):
               folder = os.path.join(self.destination,f'{lon}',f'{lats[x]}')
               os.makedirs(folder,exist_ok=True)

    def numpydate_2_datetime(self):
        tz=timezone(timedelta(hours=0))
        tid = self.dat_array.time.values[0].astype(datetime)/1e9
        tid = datetime.fromtimestamp(tid,tz).isoformat()
        return tid

    def data_read(self,last=True):
        date = self.start
        day = 0
        dat = []
        cont = True
        start = perf_counter()
        print(datetime.now().isoformat())
        while date <= self.end and cont:
            print(date)
            y,m,d = date.year,date.month,date.day
            dd = os.path.join(self.origin,str(y),str(m),str(d))
            nr_files = os.listdir(dd)
            #with MyTimer('Open NC files') as _ :
            print(dd,nr_files)
            if len(nr_files)!=24:
                cont = False
                continue
            tmp = xr.open_mfdataset(f'{dd}/*nc')
            dat.append(tmp)
            day += 1
            date += timedelta(1) 

            if day%self.days==0:
                #with MyTimer('data_read_concat') as _ :

                self.dat_array = xr.merge(dat)
                self.dat_array = self.dat_array.sortby('time')
                self.groupstart = self.numpydate_2_datetime()   
                print("Loading")                
                self.dat_array.load()
                print("Done Loading")
                self.writemask()
                print("Done writemask")
                print(date,self.dat_array.time.max(),perf_counter()-start)
                start = perf_counter()
                day = 0
                for t in dat:
                    t.close()
                    #del t
                dat = []
                self.dat_array.close()
        if dat and last:
            self.dat_array = xr.merge(dat)
            self.dat_array.load()
            self.groupstart = self.numpydate_2_datetime()
            self.writemask()
            self.dat_array.close()
            for t in dat:
                t.close()
        for x in dat:
            del x
        del dat
        
        
    def writemask(self):
        for lon in self.mask.longitude.values:
            #print("longitude",lon)
            #with MyTimer('writemask_lats_for_lon') as _ :
            select = self.dat_array.sel(longitude=lon,
                            latitude=self.mask_dict[lon])
            #with MyTimer('writemask_write_a_lon') as _ :
            self.write(lon,select)

class File_pivot(Pivot):

    # def __attrs_post_init__(self):
    #     self.create_folders()
        # self.data_read() 

    def create_folders(self):
        for lon in self.mask.longitude.values:
            os.makedirs(os.path.join(self.destination,
                            f'{lon}'),exist_ok=True)
         
    def write(self,lon,d):
        lats = self.mask_dict[lon] #d.latitude.values
        x = 0
        while x<len(lats)-self.points:
            group = d.sel(latitude=lats[x:x+self.points])

            #with MyTimer('write_helper') as _ :
            self.write_helper(lon,lats,x,group)
            x += self.points
            group.close()
        if x<len(lats):
            rest = d.sel(latitude=lats[x:])
            #with MyTimer('write_helper') as _ :
            self.write_helper(lon,lats,x,rest)
            rest.close()
        d.close()

    def write_helper(self,lon,lats,x,d):

        tid = self.groupstart
        tid = tid.replace(":","_")
        folder = os.path.join(self.destination,f'{lon}',f'{lats[x]}')
        file = os.path.join(folder,f'{tid}.nc')
        #with MyTimer('write_to_netcdf') as _ :
        d.to_netcdf(file,mode='w')
        d.close()


def create_mask(mask):
    m = []
    for lon in mask.longitude.values:
        for lat in mask.latitude.values:
            try:
                #with MyTimer('mask_sel') as _ :
                mm = mask.sel(latitude=lat,longitude=lon)
            except KeyError:
                #print('Latitude=',lat,'Longitude=',lon)
                continue
            if not np.isnan(mm.cl.values[0]):
                m.append((lat,lon))
    return m

def find_latest_date(path):
    greenwich = os.path.join(path,'0.0')
    lat = os.listdir(greenwich)
    first = os.path.join(greenwich,lat[0])
    files = os.listdir(first)
    return max(files)
    
def day_point_metric():
    # 40 år gange 1440*741 på et halvt år:334dagpunkt/s
    return days*points/runtime

if __name__ == '__main__':
    import shutil,sys
    if len(sys.argv)>=2: 
        conversion_year = int(sys.argv[-1])
    else:
        conversion_year = 2019
    print(datetime.now().isoformat())
    
    land =xr.open_dataset('download.nc')
    m = land.where(land.lsm+land.cl>0.5,drop=True)
    start = perf_counter()
    #m = m.sel(longitude=slice(0,359.75))
    #mask = create_mask(m)
    sourcepath ='F:\Data\ERA5\CDS\global'
    destination = 'F:\Data\ERA5\CDS\point2'
#    sourcepath = '/Users/au261214/projects/Swat_app/scripts/data/global/'
    #sourcepath = "/Volumes/dfs/Tech_ERA5/CDS/"
#    destination = '/Users/au261214/projects/Swat_app/scripts/data/point2/'
    f = open('timing.log','w')
    print(">>>>>>>>>>>>   days  group  runtime dp/s",file=f)
    days = 6
    group = 32
    first = False
    for year in [conversion_year]:

        #start = perf_counter()
        piv = File_pivot(destination=destination,
                    origin=sourcepath,
                    mask=m,
                    start=datetime(year,1,1),
                    end=datetime(year,12,31),
                    days=days,
                    points=group
                    )
        if first:
            #shutil.rmtree(destination)
            first = False
            #with MyTimer('Create Folders') as _ :
            piv.create_folders()
        #with MyTimer('make_mask') as _ :
        piv.make_mask_dict()
 #       with MyTimer('Data read') as _ :
        piv.data_read()
        print(">>>>>>>>>>>>   days  group  runtime dp/s")
        run = perf_counter()-start
        print (">>>>>>>>>>>>  ",days,group,run,741*31/run)
        print(">>>>>>>>>>>>  ",days,group,run,
                len(m.latitude.values)*31/run,file=f)
        del piv
    #import pdb;pdb.set_trace()
    f.close()
 #   for key,val in MyTimer.timing.items():
 #       print(key,len(val),sum(val))

def Myprint(file):
    with open(file,'w') as f:
        print(">>>>>>>>>>>>   days  group  runtime dp/s",file=f)
        while True:
            yield
            print(">>>>>>>>>>>>  ",days,group,741*31/run,file=f)
