# -*- coding: utf-8 -*-
# Program to get data from netCDF file into SQL db
# saves the latest date to have 24 hour coverage location table

import datetime
import pandas as pd
import xarray as xr
from collections import defaultdict
import os
import xarray
import attr
import time
import numpy as np
from .database import PG


class NotOnLandException(Exception):
    pass

data_dir='F:\\Data\\ERA5\\CDS\\global\\'
#data_dir='/Users/au261214/projects/Swat_app/scripts/data/global'

places = {'kaiping': {'lat':22.523669,'lng': 112.422059},
         'seest':{'lat':55.490070, 'lng':9.403019},
         'bosomtwe':{'lat':6.5, 'lng': 360-1.41},
         "ravn":{"lat" : 56.10, "lng" : 9.85},
         "arreskov" :{"lat" : 55.16, "lng" : 10.30  },
         "skanderborg" :{"lat" : 56.02, "lng" : 9.93},
         "bryrup":{"lat" : 56.02, "lng" : 9.52},
         "hinge":{"lat" : 56.255, "lng" : 9.50},
         "soeholm":{"lat" : 55.273, "lng" : 10.13},
         "furesoe":{"lat" : 55.80, "lng" : 12.41},
         "almind":{"lat" : 56.15, "lng" : 9.54 },
         "ormstrup":{"lat" : 56.33, "lng" : 9.64},
         "san_javier":{"lat": 37.791694, "lng": -0.819578},
         "san_javier_lagune":{"lat" : 37.723106, "lng": -0.784761},
         }  
def get_data_latlon_fast(locs,start,end,path=data_dir):
    res = defaultdict(pd.DataFrame)
    #sort point after lng
    lngs = defaultdict(list)
    for name,lat,lon,start,end in locs:
        lngs[lon].append(lat)
    for lon in lngs.keys():
        p = os.path.join(path,"..","point",str(lon))
        lats = [float(x) for x in os.listdir(p)]
        lats.sort()
        folders = set()
        for lat in lngs[lon]:
            folders.add(binsearch(lats,lat))
        dat = []
        for f in folders:
            folder = os.path.join(p,str(f))
            print(folder)
            files = os.listdir(folder)
            dates = [map(int,x.split('T')[0].split('-')) for x in files]
            dates = [datetime.datetime(*x) for x in dates]
            first = largest_lessthan(dates,start)
            dates = [x for x in dates if x<=end and x>=start]
            if first not in dates:
                dates.insert(0,first)
            datefiles = [f'{folder}/{x.date()}T00_00_00+00_00.nc' for x in dates]
            dat.append(xr.open_mfdataset(datefiles))
        print(dat)
        data = xr.merge(dat)
        res[name]=data.sel(latitude=lngs[lon]).to_dataframe()
    return filtrate_(res),start,end


def get_data_latlon(locs,start,end,path=data_dir):
    res = defaultdict(pd.DataFrame)
    for name,lat,lon,_ in locs:
        lat,lon = nearest(lat,lon)
        if type(start) == str:
            start = datetime.datetime(*[int(x) for x in start.split('-')])
            end = datetime.datetime(*[int(x) for x in end.split('-')])
        p = os.path.join(path,"..","point2",str(lon))
        dat = []
        lats = [float(x) for x in os.listdir(p)]
        lats.sort()
        print(p,lats)
        files = set()
        files.add(binsearch(lats,lat))

        for f in files:
            folder = os.path.join(p,str(f))
            print(folder)
            files = os.listdir(folder)
            dates = [map(int,x.split('T')[0].split('-')) for x in files]
            dates = [datetime.datetime(*x) for x in dates]
            first = largest_lessthan(dates,start)
            dates = [x for x in dates if x<=end and x>=start]
            if first not in dates:
                dates.insert(0,first)
            datefiles = [f'{folder}/{x.date()}T00_00_00+00_00.nc' for x in dates]
            dat.append(xr.open_mfdataset(datefiles))
        print(dat)
        data = xr.merge(dat)
        try:
            res[name]=data.sel(latitude=lat).to_dataframe()
        except KeyError as e:
            print("!!!!!! Location not found (lon,lat):",lon,lat)
            continue
            #raise NotOnLandException()
    return filtrate_(res),start,end

def binsearch(items,item):
    lats = items.copy()
    l = len(lats)
    while l>2:
        l_2 = l//2 #+l%2
        print(l_2,item)
        if lats[l_2] > item:
            lats = lats[:l_2+1]
        else:
            lats = lats[l_2:]
        print(lats)
        l = len(lats)
    res = lats[0] if item==lats[0] else lats[1]
    print('===========',item,res,lats)
    return res

def largest_lessthan(dates,date):
    dates.sort()
    res = [x for x in dates if x<=date]
    return res[-1] if res else dates[-1]

def nearest(lat,lon):
    lon = lon if lon >= 0 else 360+lon
    return round(lat*4)/4,round(lon*4)/4

@attr.s        
class ReadData:

    start = attr.ib()
    end = attr.ib()
    lat_lon = attr.ib()
    data_dir = attr.ib()
    agg = attr.ib()

    def __call__(self):
        return self.prepare()

    def prepare(self):
        start = self.start
        end = self.end
        res_df = defaultdict(pd.DataFrame)
        while start<=end :
            y = str(start.year)
            m = str(start.month)
            d = str(start.day)
            path = os.path.join(self.data_dir,y,m,d)
            try:
                hours = self.get_files(path,self.agg)
            except FileNotFoundError as e:
                if start==self.start:
                    start += datetime.timedelta(1)
                    self.start = start
                    continue
                else:
                    self.end = start - datetime.timedelta(1)
                    break
            print('File(s) to open',hours,start)
            for h in hours:
                era_h = xarray.open_dataset(os.path.join(path,h))
                for p,lat,lon,st,en in self.lat_lon:
                    if start<st or start>en:
                        continue
                    lon = [float(x) for x in lon]
                    lon = lon if lon >= 0 else 360. + lon
                    kp = era_h.sel(latitude=lat,longitude=lon,method='nearest')
                    res_df[p]=res_df[p].append(kp.to_dataframe())
            start += datetime.timedelta(1)

        return res_df

    def new_read(self):
        start = self.start
        end = self.end
        imean = 0
        smean = 0
        res_df = defaultdict(pd.DataFrame)
        initial = []
        second = []
        p,lat,lon,st,en = zip(*self.lat_lon)
        #lat = list(set(lat))
        #lon = list(set(lon))
        lat = [float(x) for x in lat]
        lon = [float(x) for x in lon]
        lon = [x if x >= 0 else 360. + x for x in lon]
        print(start,end,start<end)
        while start<=end :
            y = str(start.year)
            m = str(start.month)
            d = str(start.day)
            path = os.path.join(self.data_dir,y,m,d)
            try:
                hours = self.get_files(path,self.agg)
                print(hours)
            except FileNotFoundError as e:
                if start==self.start:
                    start += datetime.timedelta(1)
                    self.start = start
                    continue
                else:
                    self.end = start - datetime.timedelta(1)
                    break
            print('File(s) to open',hours,start)
            for h in hours:
                timing0 = time.perf_counter()
                era_h = xarray.open_dataset(os.path.join(path,h))
                initial.append(time.perf_counter()-timing0)
                timing1 = time.perf_counter()
                for p,la,lo,st,en in self.lat_lon:
                    if start<st or start>en:
                        continue

                    lo = lo if lo>=0 else 360+lo
                    kp = era_h.sel(latitude=la,longitude=lo,method='nearest').to_dataframe()
                    kp = kp.sort_index()
                    #sl = kp.index.get_loc(nearest(la,lo))
                    res_df[p]=pd.concat((res_df[p],kp))
                second.append(time.perf_counter()-timing1)
            start += datetime.timedelta(1)
        if initial:
            imean = sum(initial)/len(initial)
        if second:
            smean = sum(second)/len(second)
        with open('timing.txt','a') as f:
            print("Initial timing",imean,sum([x-imean for x in initial])/len(initial),file=f)
            print("Secondary timing",smean,sum([x-smean for x in second])/len(second),file=f)
        return res_df
           
    def get_files(self,path,agg):
        if agg=='day' and ('daily.nc' in os.listdir(path)):
            return ['daily.nc']
        else:
            return [x for x in os.listdir(path) if x != 'daily.nc']

def get_weather_for_location(lat_lon,startdate,enddate,data_dir=data_dir,agg='day',read=True):

    if type(startdate)==datetime.datetime:
        start = startdate
        end = enddate
    elif type(startdate)== tuple:
        start = datetime.date(*startdate)
        end = datetime.date(*enddate)
    else:
        start = startdate
        end = enddate
    #fullday = db.get_fullday_from_db('kaiping')
    # if end>fullday:
    #     end = fullday
    read_data = ReadData(start,end,lat_lon,data_dir,agg)
    if read:
        res_df = read_data.new_read()
    else:
        res_df = read_data()

    res_df = filtrate_(res_df)
    return res_df,read_data.start,read_data.end

def kelvin2celsius(df):
    return df-273.15

def celsius2fahrenheit(df):
    pass

def fahrenheit2celsius(df):
    pass

def relhum(Tc,Tdc):
    Es = (6.11*10**(7.5*Tc/(237.7+Tc)))
    E = (6.11*10**(7.5*Tdc/(237.7+Tdc)))
    return E/Es #*100

def remove_negative(df):
    return np.where(df<0.0, 0.00, df)

wet_vars = {'t2m':('t2m',lambda x: kelvin2celsius(x)),
            'd2m':('d2m',lambda x: kelvin2celsius(x)),
            'msp':('sp',lambda x: x*0.01),
            'relhum':(['t2m','d2m'],lambda df:relhum(df[df.columns[0]],df[df.columns[1]])),
            'tcc':('tcc',lambda df: remove_negative(df)),
            'ssrd':('ssrd',lambda df: remove_negative(df)),
            'tp':('tp',lambda df: remove_negative(df)*1000),
        }

def filtrate_(res_df,variables=wet_vars):
    for p in res_df.keys():
        for var,conv in variables.items():
            res_df[p][var] = conv[1](res_df[p][conv[0]])
    return res_df

def filtrate(res_df,lat_lon):            
    for p,lat,lon,_  in lat_lon:
        res_df[p]['t2m'] = res_df[p]['t2m'] - 273.15 # Convert from Kelvin to degrees C
        res_df[p]['d2m'] = res_df[p]['d2m'] - 273.15 # Convert from Kelvin to degrees C
        res_df[p]['msp'] = res_df[p]['sp'] * 0.01   # Convert from Pascals to hPa
         # some cloud cover values from ERA5 are reported as negative (but extremely low), set these to 0.00
        res_df[p]['tcc'] = np.where(res_df[p]['tcc']<0.0, 0.00, res_df[p]['tcc'])
        res_df[p]['ssrd'] = np.where(res_df[p]['ssrd']<0.0, 0.00, res_df[p]['ssrd'])
        res_df[p]['tp'] = np.where(res_df[p]['tp']<0.0, 0.00, res_df[p]['tp']*1000)
        Tc = res_df[p]['t2m']
        Tdc = res_df[p]['d2m']
        res_df[p]['relhum'] = relhum(Tc,Tdc)
# Select only required data for output
        res_df[p] = res_df[p][['u10','v10','msp','tp','t2m','relhum','d2m','tcc','ssrd']]
        
    return res_df

def fullday(end,direction=1):

    fullday = True
    while fullday:
        end += direction*datetime.timedelta(1)
        y,m,d = end.timetuple()[:3]
        try:
            day = f'{y}/{m}/{d}'
            pwd = data_dir+day
            files = os.listdir(pwd)

            fullday = len(files)>=24

        except FileNotFoundError:
            break

    end -= direction*datetime.timedelta(1)
    return end

def get_date_ranges():
    path = os.path.join(data_dir,'..\\point2\\0.0\\8.25')
    start = fullday(datetime.datetime(1979,1,1),-1)
    end = fullday(datetime.datetime(2022,1,1))
    loc_end = xarray.open_dataset(os.path.join(path,max(os.listdir(path))))
    mx = loc_end['time'].max()
    mxd = mx.data.astype(datetime.datetime)
    location_end = datetime.datetime.fromtimestamp(mxd.tolist()/1e9,
        datetime.timezone.utc).date()
    ranges = {
        'location':{'start':min(os.listdir(path)).split('T')[0],
                    'end':location_end.isoformat()},
        'time':{'start':start.date().isoformat(),
                'end':end.date().isoformat()}
    }
    return ranges

if __name__ == '__main__':
    import sys
    pw = os.environ.get("DB_PASSWORD","")
    db = PG(pw=pw)
    ranges = get_date_ranges()
    end = ranges['location']['end']
    end = datetime.datetime(*[int(x) for x in end.split('-')])
    print("old fullday",end)
    print("sys.argv",sys.argv,end)

    if len(sys.argv)==4:
        y,m,d = [int(x) for x in sys.argv[1:]]
        start = datetime.datetime(y,m,d)
    else:
        start = end
        start = datetime.datetime(start.year,start.month,start.day)
        start += datetime.timedelta(1)
    print("start date",start,type(start))


    active_places = db.get_active_places()

    begin = time.perf_counter()
    if start<=end:
        #df = get_weather_for_location(active_places,start,end)
        df,_,_ = get_data_latlon(active_places,start,end)
        
        for p,_,_,_ in active_places:
            #df[p].to_csv(f'{p}.dat', header=True, sep='\t', float_format='%.2f')
            finish = time.perf_counter()
            print(f"read time {finish-begin}")
            db.dataframe2sql(p,df[p],'replace')
            begin = time.perf_counter()
            print(f"sql time {begin-finish}")
        print('**************',end)
        db.update_location(end)
