import attr
import requests as r
from post_vars import rural,reservoir,live
@attr.s
class TestASAP:
    base_uri = attr.ib(default='https://asap-forecast.com/')

def post(uri,json):
    return r.post(uri,json=json)

class TestRural(TestASAP):
    
    def monitor(self,project):
        return [{'test':r.get(self.base_uri).status_code',
             'expected':200},
             {'test':post,
             'expected':''}]