# Script for downloading ERA5 data from the Climate Data Store (CDS)
# Dennis Trolle, 18 Sep. 2018
# note: to run this script, you must first register a CDS account
# and then follow the steps on: https://cds.climate.copernicus.eu/api-how-to

import cdsapi
import os
from datetime import date,timedelta

c = cdsapi.Client()

#area = [6.5, -1.5, 6.0, -1.0]          # North(lat), West(lon), South(lat), East(lon). Default: global. Note: Southern latitudes and western longitudes must be given as negative numbers.
area = [55.2, 10.2, 55.0, 10.3]
dates = [date(2020,1,1),date.today()-timedelta(5)]          # yyyy-mm-dd/yyyy-mm-dd
times = '00:00/01:00/02:00/03:00/04:00/05:00/06:00/07:00/08:00/09:00/10:00/11:00/12:00/13:00/14:00/15:00/16:00/17:00/18:00/19:00/20:00/21:00/22:00/23:00'.split('/')
variables = ['10m_u_component_of_wind','10m_v_component_of_wind','2m_dewpoint_temperature',
            '2m_temperature','surface_pressure','total_cloud_cover','total_precipitation',
            'surface_solar_radiation_downwards']
nc_output = 'F:\\Data\\ERA5\\CDS\\global\\'  # location and name of netcdf output file

# c.retrieve('reanalysis-era5-single-levels', {
#         'variable'      : ['10m_u_component_of_wind','10m_v_component_of_wind','2m_dewpoint_temperature','2m_temperature','surface_pressure','total_cloud_cover'],
#         'product_type'  : 'reanalysis',
#         'date'          : dates,
#         'area'          : area, # North, West, South, East. Default: global
#         'grid'          : [0.25, 0.25], # Latitude/longitude grid: east-west (longitude) and north-south resolution (latitude). Default: 0.25 x 0.25
#         'time'          : ['00:00/01:00/02:00/03:00/04:00/05:00/06:00/07:00/08:00/09:00/10:00/11:00/12:00/13:00/14:00/15:00/16:00/17:00/18:00/19:00/20:00/21:00/22:00/23:00'],
#         'format'        : 'netcdf' # Supported format: grib and netcdf. Default: grib
#                        }, nc_output)




# Get precipitation and downwards radiation
curdate = dates[1]
while curdate>dates[0]:
    datestr = f'{curdate.year}-{curdate.month:02}-{curdate.day:02}'
    datepth = f'{curdate.year}\\{curdate.month}\\{curdate.day}\\'
    os.makedirs(nc_output+datepth,exist_ok=True)
    for time in times:
        filestr = nc_output+datepth+datestr+f'_{time.replace(":","-")}.nc'
        print('>>>>>>>> '+filestr)
        if os.path.exists(filestr):
            print('File exists trying next')
            continue
        try:
            c.retrieve('reanalysis-era5-single-levels', {
                'variable'      : variables,
                'product_type'  : 'reanalysis',
                'date'          : datestr, # dates,
        #        'area'          : area, # North, West, South, East. Default: global
                'grid'          : [0.25, 0.25], # Latitude/longitude grid: east-west (longitude) and north-south resolution (latitude). Default: 0.25 x 0.25
                'time'          : time, #/01:00/02:00/03:00/04:00/05:00/06:00/07:00/08:00/09:00/10:00/11:00/12:00/13:00/14:00/15:00/16:00/17:00/18:00/19:00/20:00/21:00/22:00/23:00'],
                'format'        : 'netcdf' # Supported format: grib and netcdf. Default: grib
                               }, filestr)
            if os.path.getsize(filestr)< 16000000:
                os.remove(filestr)
                print('Small file -----> removed!')
        except Exception:
            print("No data. trying next")
            continue
    curdate -= timedelta(1)
