import psycopg2 as pg
from datetime import datetime
import attr
from sqlalchemy import create_engine
host = 'localhost'

@attr.s 
class PG:
    
    host = attr.ib(default='localhost')
    port = attr.ib(default=5432)
    user = attr.ib(default='postgres')
    db = attr.ib(default='CDS')
    schema = attr.ib(default='public')
    pw = attr.ib(default='rebus')
    remote_host = attr.ib(default='test.asap-forecast.com')
    remote_user = attr.ib(default='ec2-user')
    remote_pkey = attr.ib(default="/Users/au261214/.ssh/id_ecdsa")
    remote_port = attr.ib(default=5432)

    def __attrs_post_init__(self):
        user = f'{self.user}:{self.pw}' if self.pw else f'{self.user}'
        self.pghost = f"postgresql://{user}@{self.host}:{self.port}/{self.db}"

    def __enter__(self):

        PRIVATE_SERVER_IP='localhost'
        print(self.remote_host,self.remote_user,self.remote_pkey,self.port)
        # self.tunnel = sshtunnel.open_tunnel(
        #     (self.remote_host, 22),
        #     ssh_username=self.remote_user,
        #     ssh_pkey=self.remote_pkey,
        #     ssh_private_key_password="",
        #     remote_bind_address=(PRIVATE_SERVER_IP, self.remote_port),
        #     local_bind_address=('0.0.0.0', self.port)
        # )
        # self.tunnel.start()
        return self

    def __exit__(self,exc_type,exc_val,exc_tb):
        #self.tunnel.close()
        pass

    def execute(self,query):
        print(self.pghost)
        conn = pg.connect(self.pghost)
        cursor = conn.cursor()
        cursor.execute(query)
        conn.commit()
        conn.close()

    def insert_location(self,locs):
        vals = [f"('{k}',{locs[k]['lat']},{locs[k]['lng']})" for k in locs.keys()]
        keys = '(name,lat,lon)'
        query = f"""insert into location {keys} values {', '.join(vals)}"""
        print(query) 
        self.execute(query)

    def create_location (self,name,lat,lon,active=True,swat=True,wet=False,elevation=135,schema='public'):
        last_era5_fullday = self.get_query("select max(last_era5_fullday) from location;")[0][0]
        query = f"""insert into location 
                    (name,lat,lon,active,swat,wet,elevation,schema,last_era5_fullday) values 
                    ('{name}',{lat},{lon},{active},{swat},{wet},{elevation},'{schema}',
                    '{last_era5_fullday}');"""
        self.execute(query)

    def update_location(self,fullday):
        query = f"update location set last_era5_fullday = '{fullday.strftime('%Y-%m-%d')}' where active;"
        self.execute(query)

    def get_query(self,query):
        print(self.pghost,query)
        self.conn = pg.connect(self.pghost)
        cursor = self.conn.cursor()
        cursor.execute(query)
        lat_lon = cursor.fetchall()
        self.conn.close()
        return lat_lon

    def get_active_places(self,product=None):
        if product:
            query = f"select name,lat,lon,id from location\
                 where active and {product}"
        else:
            query = f"select name,lat,lon,id from location\
                 where active"
        return self.get_query(query)

    def get_full_day(self):
        query = f"select max(last_era5_fullday) from location where active"
        res = self.get_query(query)
        print("PG",res)
        if res:
            if res[0][0]:
                return res[0][0]
            else:
                return datetime.now().date()
        else:
            return res

    def get_fullday_from_db(self,place):
        query = f"select last_era5_fullday from location where name='{place}'"
        res = self.get_query(query)
        print("PG",res)
        if res:
            return res[0][0]
        else:
            return res

    def dataframe2sql(self,name,df,replace='replace'):
        print("Save dataframe",self.pghost,name,replace)
        from io import StringIO
        buf = StringIO()
        conn = pg.connect(self.pghost)
        cursor = conn.cursor()
        try:
            # df.to_sql(name=name,con=conn,if_exists='fail',index=True,index_label='time',
            #     method='multi',chunksize=10,schema=f'"{self.schema}"') 
            cursor.execute(f'delete from "{self.schema}"."{name}" *')
            conn.commit()
            df.to_csv(buf,sep=',',index=True,header=False)
            columns = list(df.columns)
            print("Starting to copy",columns,df.index)
            buf.readline()
            print("Readline",buf.readline())
            self.copy_from(buf,f'"{self.schema}"."{name}"',sep=',')
                #columns=columns)
        except Exception as e:
            print(e)
            self.to_sql(name,df)
        # eng = create_engine(self.pghost,executemany_mode='values',
        #     executemany_values_page_size=10000)
        # df.to_sql(name,eng,index=False,schema=self.schema,
        #         chunksize=5000,if_exists=replace,method='multi')
        print("saved")

    def to_sql(self,name,df,if_exists='replace'):
        eng = create_engine(self.pghost,executemany_mode='values',
                executemany_values_page_size=10000)
        df.to_sql(name=name,con=eng,if_exists=if_exists,index=True,
                method='multi',chunksize=1000,schema=f'{self.schema}')

    def sql2dataframe(self,query):
        print("Save dataframe",self.pghost)
        eng = create_engine(self.pghost)
        df = pg.read_sql(query,eng)
        print("saved")
        return df

    def copy_from(self,buf,table,columns=None,sep=','):
        conn = pg.connect(self.pghost)
        cursor = conn.cursor()
        buf.seek(0)
        cursor.copy_from(buf,table,columns=columns,sep=',')
        conn.commit()
        conn.close()