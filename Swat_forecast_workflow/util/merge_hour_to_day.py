import xarray
import os

def merge(path):
    for f in os.listdir(path):
        p = os.path.join(path,f)
        if os.path.isdir(p):
            merge(p)
        elif len(os.path.isdir(path))==24:
            merge_day(path)
            break

def merge_day(path):
    ds = []
    print(path)
    p,d = os.path.split(path)
    p,m = os.path.split(p)
    p,y = os.path.split(p)
    for i in range(24):
        ds.append(xarray.open_dataset(f'{path}/{y}-{int(m):02}-{int(d):02}_{i:02}-00.nc'))
    ds2 = xarray.concat(ds,'time')
    ds2.to_netcdf(f'{path}/daily.nc') 

data_dir='F:\\Data\\ERA5\\CDS\\global\\'
if __name__ == '__main__':
    merge(data_dir)