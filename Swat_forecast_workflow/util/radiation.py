from math import pi,cos,sin,asin,acos

deg2rad = pi/180
rad2deg = 180/pi
solar = 1350.
eclips = 23.439*deg2rad
tau = 0.7
aozone = 0.09

def shortwave_radiation(hh,yday,dlon,dlat,cloud):
    coszen = cos(deg2rad*solar_zenith_angle(yday,hh,dlon,dlat))
    if (coszen <= 0.0):
        coszen = 0.0
        qatten = 0.0
    else:
        qatten = tau**(1./coszen)
    
    qzer  = coszen * solar
    qdir  = qzer * qatten
    qdiff = ((1.-aozone)*qzer - qdir) * 0.5
    qtot  =  qdir + qdiff

    #  from now on everything in radians
    rlat = deg2rad*dlat

    yrdays=365.
    eqnx = (yday-81.)/yrdays*2.*pi
    #  sin of the solar noon altitude in radians :
    sunbet=sin(rlat)*sin(eclips*sin(eqnx))+cos(rlat)*cos(eclips*sin(eqnx))
    #  solar noon altitude in degrees :
    sunbet = asin(sunbet)*rad2deg

    #  radiation as from Reed(1977), Simpson and Paulson(1979)
    #  calculates SHORT WAVE FLUX ( watt/m*m )
    #  Rosati,Miyakoda 1988 ; eq. 3.8
    #  clouds from COADS perpetual data set
    #if 1
    qshort  = qtot*(1-0.62*cloud + .0019*sunbet)
    if (qshort > qtot ):
        qshort  = qtot
    
    #else
    #  original implementation
    if(cloud < 0.3):
        qshort  = qtot
    else:
        qshort  = qtot*(1-0.62*cloud + 0.0019*sunbet)

    return qshort
   
def solar_zenith_angle(yday,hh,dlon,dlat):
    rlon = deg2rad*dlon
    rlat = deg2rad*dlat

    yrdays=365.25

    th0 = 2.*pi*yday/yrdays
    th02 = 2.*th0
    th03 = 3.*th0
    #  sun declination :
    sundec = 0.006918 - 0.399912*cos(th0) + 0.070257*sin(th0)         \
            - 0.006758*cos(th02) + 0.000907*sin(th02)                 \
            - 0.002697*cos(th03) + 0.001480*sin(th03)
    #  sun hour angle :
    thsun = (hh-12.)*15.*deg2rad + rlon

    #  cosine of the solar zenith angle :
    coszen = sin(rlat)*sin(sundec)+cos(rlat)*cos(sundec)*cos(thsun)
    if (coszen < 0): coszen = 0


    return rad2deg*acos(coszen)

   

