
climate_anydesk = dict(
    host = 'localhost',
    port = 5433,
    user = 'postgres',
    db = 'CDS',
    schema = 'public',
    pw = 'U8hTVoSdAZ0c',
)
climate_local = dict(
    host = 'localhost',
    port = 5432,
    user = 'postgres',
    db = 'CDS',
    schema = 'public',
    pw = 'rebus',
)
frontend_test = dict(
    host = 'localhost',
    port = 5435,
    user = 'postgres',
    db = '',
    schema = 'public',
    pw = 'Swat.cat5',
)
frontend_asap_forecast = dict(
    host = 'localhost',
    port = 5434,
    user = 'postgres',
    db = '',
    schema = 'public',
    pw = 'eerrymypfido',
)
local_swat = dict(
    host = 'localhost',
    port = 5432,
    user = 'postgres',
    db = 'swat',
    schema = '',
    pw = '',
)