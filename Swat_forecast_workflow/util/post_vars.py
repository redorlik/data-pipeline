from datetime import datetime,timedelta

end = datetime.now()+timedelta(7)
start = end - timedelta(1)
end_t = int(end.timestamp())
start_t = int(start.timestamp())

test2 ={
	"vars":[{
	    "landscape":"waterbody",
	    "featureId":"",
	    "parameters":[
		    {
		    	"parameter":"abiotic_water_sO2W$hypsographic",
		    	"timeStart":start_t,
		    	"timeEnd":end_t,
		    	"depthFrom":-10.0,
		    	"depthTo":-10.0
		    	
		    },
           {
		    	"parameter":"temp",
		    	"timeStart":start_t,
		    	"timeEnd":end_t,
		    	"depthFrom":-10.0,
		    	"depthTo":-10.0
		    	
		    },
            {
		    	"parameter":"Hice",
		    	"timeStart":start_t,
		    	"timeEnd":end_t,
		    	"depthFrom":-10.0,
		    	"depthTo":-10.0
		    	
		    }
	    ]
	}]
}
rural_vars_kaiping = {"vars":[

    {"landscape":"stations",
     "featureId":"station1",
      "parameters":[
    {
       "parameter":"precip",
      "timeStart":start.date(),#"2020-06-01",
      "timeEnd" : end.date(), #"2020-06-15"
    },   
    {
       "parameter":"flo_out$statistics",
      "timeStart":start.date(),#"2020-06-01",
      "timeEnd" :end.date(), #"2020-06-19"
    }]
    }
    ,
    {"landscape":"hru",
     "featureId":"11",
      "parameters":[
    {
       "parameter":"precip",
      "timeStart":start.date(),#"2020-06-01",
      "timeEnd" :end.date(), #"2020-06-19"
    },{
       "parameter":"latq",
      "timeStart":start.date(),#"2020-06-01",
      "timeEnd" :end.date(), #"2020-06-19"
    },{
       "parameter":"latq",
      "timeStart":start.date(),#"2020-06-01",
      "timeEnd" :end.date(), #"2020-06-19"
    },{
       "parameter":"perc",
      "timeStart":start.date(),#"2020-06-01",
      "timeEnd" :end.date(), #"2020-06-19"
    }]
    }
]
}
station={"vars":[

    {"landscape":"stations",
     "featureId":"station1",
      "parameters":[
    {
       "parameter":"precip",
      "timeStart":start.date(),#"2020-06-01",
      "timeEnd" : end.date(), #"2020-06-15"
    },   
    {
       "parameter":"flo_out$statistics",
      "timeStart":start.date(),#"2020-06-01",
      "timeEnd" :end.date(), #"2020-06-19"
    },
    {"landscape":"stations",
     "featureId":"station2",
      "parameters":[
    {
       "parameter":"precip",
      "timeStart":start.date(),#"2020-06-01",
      "timeEnd" :end.date(), # "2020-06-15"
    },   
    {
       "parameter":"flo_out$statistics",
      "timeStart":start.date(),#"2020-06-01",
      "timeEnd" :end.date(), #"2020-06-19"
    }]
    }]
    }
]
}
rural_vars_vejle = {"vars":[

    {"landscape":"stations",
     "featureId":"station1",
      "parameters":[
    {
       "parameter":"precip",
      "timeStart":start.date(),#"2020-06-01",
      "timeEnd" :end.date(), # "2020-06-15"
    },   
    {
       "parameter":"flo_out$statistics",
      "timeStart":start.date(),#"2020-06-01",
      "timeEnd" :end.date(), #"2020-06-19"
    }]
    }

]
}
start = 1610438557 #datetime.datetime(2020, 11, 9, 3, 22, 9).timestamp()
end = 1611320557 #datetime.datetime(2020,12 , 16, 16, 38, 9).timestamp()
datetime.now()
fejl_request = {"vars":[
    {"landscape":"liveData",
     "featureId":"buoy1",
     "parameters":[
        {"parameter":"Phycocyanin concentration","timeStart":start,"timeEnd":end,"depthFrom":0,"depthTo":-1},
         {"parameter":"Chlorophyll-a. concentration","timeStart":start,"timeEnd":end,"depthFrom":0,"depthTo":-1},
        {"parameter":"Turbidity","timeStart":start,"timeEnd":end,"depthFrom":0,"depthTo":-1},
        {"parameter":"Total Suspended Solids","timeStart":start,"timeEnd":end,"depthFrom":0,"depthTo":-1},
        {"parameter":"Temperature1", "timeStart":start,"timeEnd":end,"depthFrom":0,"depthTo":-1},
         {"parameter":"Temperature1", "timeStart":start,"timeEnd":end,"depthFrom":0,"depthTo":-1},
        {"parameter":"Temperature$all","timeStart":start,"timeEnd":end,"depthFrom":0,"depthTo":-40},
        {"parameter":"Chlorophyll-a. fluorescence", "timeStart":start,"timeEnd":end,"depthFrom":0,"depthTo":-1},
 #       {"parameter":"Phycocyanin fluorescence","timeStart":start,"timeEnd":end,"depthFrom":0,"depthTo":-40}
    
     ]}]}
langsom_request = {"vars":[
    {"landscape":"liveData",
     "featureId":"buoy1",
     "parameters":[
        {"parameter":"Phycocyanin concentration","timeStart":start,"timeEnd":end,"depthFrom":0,"depthTo":-1},
        {"parameter":"Chlorophyll-a. concentration","timeStart":start,"timeEnd":end,"depthFrom":0,"depthTo":-1},
        {"parameter":"Turbidity","timeStart":start,"timeEnd":end,"depthFrom":0,"depthTo":-1},
        {"parameter":"Total Suspended Solids","timeStart":start,"timeEnd":end,"depthFrom":0,"depthTo":-1},
        {"parameter":"Temperature1", "timeStart":start,"timeEnd":end,"depthFrom":0,"depthTo":-1},
        {"parameter":"Temperature$all","timeStart":start,"timeEnd":end,"depthFrom":0,"depthTo":-40},
     #   {"parameter":"Chlorophyll-a. fluorescence (RFU)", "timeStart":start,"timeEnd":end,"depthFrom":0,"depthTo":-1},
     #   {"parameter":"Phycocyanin fluorescence (RFU)","timeStart":start,"timeEnd":end,"depthFrom":0,"depthTo":-40}
     ]}]}
test4 = {
	"vars":[{
	    "landscape":"waterbody",
	    "featureId":"",
	    "parameters":[
		    {
		    	"parameter":"abiotic_water_sO2W$observations",
		    	"timeStart":1591032215,
		    	"timeEnd":1591423415,
		    	"depthFrom":-0.5,
		    	"depthTo":-33.0
		    	
		    }
	    ]
	},{
	    "landscape":"waterbody",
	    "featureId":"",
	    "parameters":[
		    {
		    	"parameter":"abiotic_water_sO2W$uncertianties",
		    	"timeStart":1591032215,
		    	"timeEnd":1591423415,
		    	"depthFrom":-0.5,
		    	"depthTo":-33.0
		    	
		    }
	    ]
	},
        {
	    "landscape":"stations",
	    "featureId":"station1",
	    "parameters":[
            {
		    	"parameter":"abiotic_water_sO2W$simulationPerformUpper",
		    	"timeStart":1578504215,
		    	"timeEnd":1591637015
		    	
		    },
		    {
		    	"parameter":"temp$statistics",
		    	"timeStart":1578504215,
		    	"timeEnd":1591637015
		    	
		    }, {
		    	"parameter":"temp$dynamics",
		    	"timeStart":1578504215,
		    	"timeEnd":1591637015,
                "depthFrom": -0.1,
                "depthTo":-32.
		    	
		    }
	    ]
	}]
}

test_stations= {
	"vars":[{
	    "landscape":"stations",
	    "featureId":"station1",
	    "parameters":[
             {
		    	"parameter":"abiotic_water_sO2W$simulationPerformUpper",
		    	"timeStart":1578504215,
		    	"timeEnd":1591637015
		    	
		    },
		    {
		    	"parameter":"temp$statistics",
		    	"timeStart":1578504215,
		    	"timeEnd":1591637015
		    	
		    }
           
	    ] 
	}]
}
test_obs =  {
	"vars":[{
	    "landscape":"waterbody",
	    "featureId":"",
	    "parameters":[
		    {
		    	"parameter":"abiotic_water_sO2W$observations",
		    	"timeStart":1591032215,
		    	"timeEnd":1591423415,
		    	"depthFrom":-0.5,
		    	"depthTo":-33.0
		    	
		    }
	    ]
	}]
}

live = [langsom_request,fejl_request]
reservoir = [test_obs,test_stations,test4]
rural = [test2,rural_vars_vejle,rural_vars_kaiping]