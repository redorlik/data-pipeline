# -*- coding: utf-8 -*-
"""
Created on Tue Feb  1 14:14:24 2022
"""

from datetime import datetime, timedelta
import os
import os.path
import netCDF4
import xml.etree.ElementTree as ET
import numpy as np
import shutil
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly


def checkFolder(pathToCheck):
    # Check whether the specified path exists or not
    isExist = os.path.exists(pathToCheck)
    if not isExist:
        # Create a new directory because it does not exist
        os.makedirs(pathToCheck)
        print("The directory %s was created" % pathToCheck)
    return


def copyHotstartFile(method, app_config):
    if method == "To_HotstartDirectory":
        shutil.copy(
            os.path.join(app_config["rootModel"], app_config["nc_restart"]),
            app_config["rootHotstartFiles"],
        )
        print("restart.nc is copied to the Hotstart Directory")

    elif method == "To_ModelDirectory":
        shutil.copy(
            os.path.join(app_config["rootHotstartFiles"], app_config["nc_restart"]),
            app_config["rootModel"],
        )
        print("restart.nc is copied to the Model Directory")

    return


def nc_update_values_main(app_config,root):
    #  the levels from the netcdf file is extracted which will the form the basis
    # for a search based on the depths of the observed profile.
    netcdf_levels = extract_netcdf_levels(
        root, "restart.nc"
    )
    # target_fmt = forecast_model_start_date

    # -----------------initialize plotting if flag is True---------------------#
    if app_config["assimilation_plot_data"] == True:

        count_obsVars = len(app_config["obsvar"])

        if count_obsVars >= 3:
            i_cols = 3
        else:
            i_cols = count_obsVars
        i_rows = get_dimensions(n=count_obsVars, UserDefinedCols=count_obsVars)

        fig = make_subplots(
            rows=i_rows, cols=i_cols, subplot_titles=[f"<{x}>" for x in app_config["obsvar"]]
        )

    # ---------------------------Getting data ---------------------------------#

    i_col = 1
    i_row = 1
    count = 1
    for obs,obs_dict_vals in app_config["obs_dict"].items():
        print(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
        print("Processing: %s" % obs)

        # based on a target date we extract the depths and values associated to the profile
        (
            days,
            dt_profile,
            selected_profile_values,
            selected_profile_depths,
        ) = obs_find_nearest_profile(obs_dict_vals, app_config["Datetime_hotstart"])

        if app_config["assimilation_day_threshold"] < days:
            print(
                "values are not assimilated due to the extent of temporal difference between profile and target date"
            )

        else:
            print("values are assimilated...")
            restart_file = os.path.join(root,"restart.nc")
            flag, initDict = nc_update_values(
                restart_file,
                obs,
                netcdf_levels,
                selected_profile_depths,
                selected_profile_values,
            )

            if (flag):  # meaning no Error in looking for the variable in the restart.nc

                # plotting:
                if app_config["assimilation_plot_data"]:

                    # producing the right index of columns and rows for plotting
                    if count <= i_cols:
                        i_col = count
                    else:
                        count = 1
                        i_col = count
                        i_row += 1
                    # call function:
                    fig = plotAssimilatedProfilesInit(
                        obs,
                        dt_profile,
                        initDict,
                        selected_profile_values,
                        selected_profile_depths,
                        netcdf_levels,
                        fig,
                        i_col,
                        i_row,
                    )
                    # print(i_row,i_col)
                    count += 1

    # plotting
    if flag:  # meaning no Error in looking for the variable in the restart.nc
        if app_config["assimilation_plot_data"]:
            print(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
            print("Assimilated profiles are generated as html")
            print(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
            plotAssimilatedProfilesRender(app_config, fig)

    return initDict


def get_dimensions(n=3, UserDefinedCols=1):

    CalculatedRows = int(round(n / UserDefinedCols, 0))
    if CalculatedRows * UserDefinedCols < n:
        CalculatedRows = CalculatedRows + 1
    return CalculatedRows


def plotAssimilatedProfilesInit(
    obs, dt_profile, initDict, vals, depths, netcdf_levels, fig, i_col, i_row
):

    plotType = 1

    if plotType == 1:

        showLegend = True
        if i_col * i_row > 1:
            showLegend = False

        fig.add_trace(
            go.Scatter(
                x=initDict[obs]["OldInitialState"],
                xaxis="x1",
                y=initDict[obs]["Depths"],
                name="OldInitState",
                # fill='tonexty',
                mode="lines+markers",
                showlegend=showLegend,
                legendgroup="OldInitState",
                # visible = "legendonly",
                # hoverinfo ="skip",
                line=dict(
                    color=("red"),
                    width=1,
                ),
            ),
            row=i_row,
            col=i_col,
        )

        fig.add_trace(
            go.Scatter(
                x=initDict[obs]["NewInitialState"],
                xaxis="x1",
                y=initDict[obs]["Depths"],
                name="Profile being assimilated",
                # fill='tonexty',
                mode="lines+markers",
                showlegend=showLegend,
                legendgroup="NewInitState",
                # visible = "legendonly",
                # hoverinfo ="skip",
                line=dict(
                    color=("green"),
                    width=1,
                ),
            ),
            row=i_row,
            col=i_col,
        )

        fig.add_annotation(
            xref="paper",
            yref="paper",
            x=0.01,
            y=0.9,
            text="Profile date: " + str(dt_profile.strftime("%Y-%m-%d %H:%M:%S")),
            showarrow=False,
            row=i_row,
            col=i_col,
        )

        fig.update_yaxes(title_text="Depth (m)", row=i_row, col=i_col)

    if plotType == 2:
        fig.add_trace(
            go.Scatter(
                x=vals,
                xaxis="x1",
                y=depths,
                name=obs,
                # fill='tonexty',
                mode="lines+markers",
                showlegend=False,
                # visible = "legendonly",
                # hoverinfo ="skip",
                line=dict(
                    color=("rgb(47,79,79)"),
                    width=1,
                ),
            ),
            row=i_row,
            col=i_col,
        )

        fig.add_trace(
            go.Scatter(
                x=[vals[0] for z in vals],
                xaxis="x1",
                y=netcdf_levels,
                name="model layer center grids",
                # fill='tonexty',
                mode="markers",
                showlegend=False,
                # visible = "legendonly",
                # hoverinfo ="skip",
            ),
            row=i_row,
            col=i_col,
        )

        fig.update_yaxes(title_text="Depth (m)", row=i_row, col=i_col)

    return fig


def plotAssimilatedProfilesRender(app_config, fig):

    fig.update_layout(
        title_text="Initial states (mapped to model variable names) before and after assimilation <br> Hotstart date: %s"
        % app_config["Datetime_hotstart"].strftime("%Y-%m-%d %H:%M:%S"),
        title_x=0.5,
    )
    plotly.offline.plot(
        fig,
        filename=os.path.join(
            app_config["assimilationRootPlots"], "assimilatedProfiles.html"
        ),
    )

    return


def update_restart_file_timestamp(app_config, offeset=-1):

    restart_file = os.path.join(app_config["rootModel"], app_config["nc_restart"])
    dset = netCDF4.Dataset(restart_file, "r+")

    t_date = dset.variables["time"].units.split(" ")[2]
    t_time = dset.variables["time"].units.split(" ")[3]

    t_datetime = datetime.strptime(str(t_date + " " + t_time), "%Y-%m-%d %H:%M:%S")
    t_datetime_new = t_datetime + timedelta(days=offeset)
    # t_datetime_new = t_datetime.replace(hour=10, minute=00)

    t_datetime_str = t_datetime_new.strftime("%Y-%m-%d %H:%M:%S")
    print(
        "The restart file datetime reference was offset to: %s to enable simulation"
        % t_datetime_str
    )
    dset.variables["time"].units = str("seconds since " + t_datetime_str)

    dset.close()

    return


def nc_update_values(
    restart_file, obs, netcdf_levels, selected_profile_depths, selected_profile_values
):
    # the restart netcdf file is opened to be updated:
    dset = netCDF4.Dataset(restart_file, "r+")

    initialStates = None
    obsError = False
    try:
        dset.variables[obs]
    except KeyError:
        print("The variable %s is not possible to assimilate. Please review" % obs)
        obsError = True

    if not obsError :  # meaning no error

        initialStates = {}

        # ----------------------getting the current initial values-------------#
        oldInitState = np.array(dset.variables[obs][..., 0, 0]).flatten().tolist()

        # ---------------------updating initial values-------------------------#
        # the profile data is snapped to the nearest depth in the nc file
        nc_profile_depth = []
        nc_profile_val = []

        for layer, depth in enumerate(netcdf_levels):
            # if depth is below a cutoff value then:
            d, v = nc_find_nearest_depth(
                selected_profile_depths, selected_profile_values, depth
            )

            nc_profile_depth.append(depth)
            nc_profile_val.append(v)

            # updating the restartfile with observation data forced in

            dset.variables[obs][:, layer, :] = v
            # uniform temperature across the water column
            # dset[obsvar][:]=4.0

        initialStates[obs] = {
            "OldInitialState": oldInitState,
            "NewInitialState": nc_profile_val,
            "Depths": nc_profile_depth,
        }

        # To DO: creating a flag if assimilation has occured:
    dset.close()  # if you want to write the variable back to disk

    return obsError, initialStates


def nc_find_nearest_depth(depth_array, value_array, lookup_depth):
    depth_array = np.asarray(depth_array)
    idx = (np.abs(depth_array - lookup_depth)).argmin()
    return depth_array[idx], value_array[idx]


def extract_netcdf_levels(root_model, nc_restart):
    # we read in the restart netcdf file to get the depth distribution, which will
    # then form the basis for setting values according to the observed values
    with netCDF4.Dataset(os.path.join(root_model, nc_restart)) as nc:
        nctime = nc.variables["time"]

        # h is the layer thichness
        h = np.array(nc.variables["h"][..., 0, 0])

        t_centers = nctime[:]
        time_unit = nctime.units
        restart = (netCDF4.num2date(t_centers, time_unit))[0]
        restart_time = restart.strftime("%Y-%m-%d %H:%M:%S")
        print("The reference date in the restart.nc is: %s" % restart_time)

        h_cumsum = h.cumsum(axis=1)
    # Centres (all)
    zs_model = h_cumsum - h_cumsum[:, -1, np.newaxis] - h / 2
    zprof = zs_model[0]

    # plt.plot(h[0], zprof)
    # plt.xlabel("layer thickness (m)")
    # plt.ylabel("depth (m)")
    # plt.show()

    return zprof


def initialize_observations(app_config):

    tree = ET.parse(
        os.path.join(str(app_config["rootObs"]), str(app_config["xml_config"]))
    )

    try:
        xml_parameters = tree.find("observations")

        obsvar_modelvar_dict = {}

        for i in xml_parameters:
            # placing xml attributes about the observations and associated model variable in a dict
            obsfile = i.attrib["source"].split("/")[-1]
            model_variable = i.attrib["modelvariable"]

            vals = read_obs_observations(obsfile, app_config)

            vals["obsfile"] = obsfile

            obsvar_modelvar_dict[model_variable] = vals

    except:
        print("no observations is included in the xml file chosen")
    app_config["obs_dict"] = obsvar_modelvar_dict
    return obsvar_modelvar_dict


def read_obs_observations(obsfile, app_config):

    obs_file_path = os.path.join(str(app_config["rootObs"]), obsfile)

    obs_dict = {}
    obs_lst_dt = []
    obs_lst_depth = []
    obs_lst_vals = []
    with open(obs_file_path, "rU") as f:
        # while 1:
        for l in f:
            # l = f.readline()
            if l.strip() == "":
                break
            time = l[:19]
            depth, obs = l[19:].strip().split()

            obs_lst_dt.append(datetime.strptime(time, "%Y-%m-%d %H:%M:%S"))
            obs_lst_depth.append(float(depth))
            obs_lst_vals.append(float(obs))

    obs_dict["datetime"] = np.array(obs_lst_dt)
    obs_dict["depth"] = np.array(obs_lst_depth)
    obs_dict["values"] = np.array(obs_lst_vals)

    return obs_dict


def obs_find_nearest_profile(obs_dict_vals, target_dt, mode="all"):

    dt_array = obs_dict_vals["datetime"]

    dt_identified = min(dt_array, key=lambda x: abs(x - target_dt))

    print(
        "The profile closest to the target datetime: \n %s" % target_dt
        + "\n is: \n %s" % dt_identified
    )

    if mode != "all":
        return dt_identified
    else:
        delta_days = abs((target_dt - dt_identified).days)
        print("Days between profile and target date are: %s" % delta_days)

        valid = np.array(
            [dt_identified <= t <= dt_identified for t in dt_array], dtype=bool
        )
        # times = [t for t, v in zip(dt_array, valid) if v]
        profile_depths = obs_dict_vals["depth"][valid]
        profile_values = obs_dict_vals["values"][valid]

        # import matplotlib.pyplot as plt
        # plt.plot(profile_values, profile_depths)
        # plt.show()

        return delta_days, dt_identified, profile_values, profile_depths


###############################################################################
