# -*- coding: utf-8 -*-
"""
About:
"""

__author__ = "Anders Nielsen"
__copyright__ = "Copyright 2022"
__credits__ = ["Anders Nielsen"]
__license__ = "GPL"
__version__ = "0.0.9"
__maintainer__ = "--"
__email__ = "--"
__status__ = "Beta"

# Er der ekstra options nødvendige i forhold til swat
#     observationer (XML/gql)
#     assimillerings periode
# Tilvejebring nødvendige options CLI/config fil
import os
import os.path
import copy
import time
from datetime import datetime, timedelta

from .gotm_utils import (gotm_yaml,run_model,
        get_info_from_netcdf_file)

from .ASAP_hotstart_utils import (copyHotstartFile,
    checkFolder,obs_find_nearest_profile,
    initialize_observations,nc_update_values_main,
    update_restart_file_timestamp)
import json


# To make this operational I need :
#   A gql interface to observations
#   A WET subclass (run warmup to last observation day, 
#       assimilate obs, run hotstart)


def update_and_run(path, start, end, output_start, output_end, output, load, missing_value):
    print("the model will run from: %s to: %s" % (start, end))

    # updating the model configuration files:
    gotm_catalog_change = {
        "time/start": start,
        "time/stop": end,
        "output/output/use": output,
        "output/output/time_unit": "hour",
        "output/output/time_start": output_start,
        "output/output/time_stop": output_end,
        "restart/load": load,
        "restart/allow_missing_variable": missing_value,
        # "surface/precip/flux_impact"       : True # in case with varying water level
        # "surface/precip/calc_evaporation"  : True # in case with varying water level
    }
    
    gotm_yaml().updateyaml(path, gotm_catalog_change, "gotm.yaml")

    # executing the model run
    cmd = "docker run  --rm \
            -v {path}:/data --name wet redorlik/gotm-wet2020".format(
        path=path
    )
    run_model(cmd, path)

def init(app_config):
    # Defining temporal periods
    app_config['dt_days_warmup'] = 365 * 5
    app_config['dt_days_forecast'] = 100  # should come dynamically from the available meteorological forecast data


    dt_now = datetime.now().replace(microsecond=0)
    Datetime_target = "2016-08-14 00:00:00"  # should be the current timestamp (datetime.datetime.now()) and will be used to find the closest observation of waterlevel
    Datetime_targetFmt = datetime.strptime(Datetime_target, "%Y-%m-%d %H:%M:%S")

    # adding it to applicaiton dict
    app_config["Datetime_targetFmt"] = Datetime_targetFmt

    # -----------------------------------------------------------------------------#
    # STEP1:  Check if the folder for hotstart (restart.nc) files exists
    # -----------------------------------------------------------------------------#

    rootHotstartFiles = os.path.normpath(app_config["rootHotstartFiles"])

    checkFolder(rootHotstartFiles)

    # -----------------------------------------------------------------------------#
    # STEP2:  Read the observation data to get the datetime of observations
    #         closest to current datetime
    # -----------------------------------------------------------------------------#
    """
        Ideally, this should be flexible to allow pulls from APIs to e.g. IoT data
        Currently, the observations are imported from .obs files (from Ravn)
    """
    obs_dict = initialize_observations(app_config)

    # adding it to applicaiton dict

    # -----------------------------------------------------------------------------#
    # STEP2b: We extract the observed profile date closest to the target date which
    #         allows us to use this date as date for running forecasts
    # -----------------------------------------------------------------------------#

    # There is a potential for different datetimes between sampled profiles if more
    # variables are subject to assimilation.
    # To define only one datetime as representative we take the most frequent one

    profileDateslst = []
    for obs in app_config["obsvar"]:
        print(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
        print("Processing: %s" % obs)
        obs_dict_vals = app_config["obs_dict"][obs]

        closestProfile = obs_find_nearest_profile(
            obs_dict_vals, app_config["Datetime_targetFmt"], mode="date"
        )
        profileDateslst.append(closestProfile)

    closestProfileDate = max(set(profileDateslst), key=profileDateslst.count)

    # adding it to applicaiton dict
    app_config["Datetime_closestProfile"] = closestProfileDate

    # -----------------------------------------------------------------------------#
    # STEP2c: Choosing the approach:
    #           "closestProfile": hotstart will be performed from the profile date
    #                             closest to the current datetime.
    #           "currentDate"   : hotstart will be performed from the current datetime.
    # -----------------------------------------------------------------------------#

    if app_config["assimilation_approach"] == "closestProfile":
        app_config["Datetime_hotstart"] = app_config["Datetime_closestProfile"]

    elif app_config["assimilation_approach"] == "currentDate":
        app_config["Datetime_hotstart"] = app_config["Datetime_targetFmt"]


    # -----------------------------------------------------------------------------#
    # STEP3:  Choose if the model run from the last forecast should be brought
    # up to date based on a longer warm-up period or from the latest restart.nc
    # -----------------------------------------------------------------------------#
    """
        We don't save the run of these "up-to-date" simulations to a output.nc,
        as we only need to restart.nc where the simulation ends. Hence, the flag:
        "output/output/use" in the gotm.yaml is set to False
    """


    MODE_forecast = "use_warmup_to_update"  # use_restart_to_update

# ----------------------- modelrun with warmup period -------------------------#
def use_warmup(app_config):
    print(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
    print("...Getting up to present via warmup period ...")

    # date configurations
    model_start_date = app_config["Datetime_hotstart"] - timedelta(
        days=dt_days_warmup
    )  # the warmup period
    model_end_date = app_config["Datetime_hotstart"]

    kw = {
        "path": app_config["path"],
        "start": model_start_date,
        "end": model_end_date,
        "output_start": copy.deepcopy(model_start_date),
        "output_end": copy.deepcopy(model_end_date),
        "output": False,
        "load": False,
        "missing_value": False,
    }
    update_and_run(**kw)
    # ----------------!!!Assimilating data from observations!!!----------------#
    print("... Assimilating data from observations ...")
    nc_update_values_main(app_config)

    # -----------!!!copy hotstart file INTO the Hotstart folder!!!-------------#
    copyHotstartFile("To_HotstartDirectory", app_config)


# ------------------------getting up to present from latest run----------------#


def use_restart(app_config):
    print(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
    print("... Getting up to present from last run via hotstart ...")

    # -----------!!!copy hotstart file INTO the Model folder!!!-------------#
    copyHotstartFile("To_ModelDirectory", app_config)

    # Getting info about the hotstart file:
    dt_hotstart = get_info_from_netcdf_file(
        app_config["rootModel"], app_config["nc_restart"], app_config["obs_dict"]
    )
    # formatting:
    dt_hotstart = datetime.strptime(dt_hotstart, "%Y-%m-%d %H:%M:%S")

    # check how the date of the hotstart file compare to the target date
    dt_diff = (app_config["Datetime_hotstart"] - dt_hotstart).days
    print(
        "The temporal window from hotstart file to target datetime is: %s days"
        % dt_diff
    )

    if app_config["Datetime_hotstart"] <= dt_hotstart:
        print(":::::::::::::::::::::::::::::::::::::::::::")
        update_restart_file_timestamp(app_config, offeset=-1)
        dt_hotstart = get_info_from_netcdf_file(
            app_config["rootModel"], app_config["nc_restart"], app_config["obs_dict"]
        )
        dt_hotstart = datetime.strptime(dt_hotstart, "%Y-%m-%d %H:%M:%S")

    # date configurations
    model_start_date = dt_hotstart
    model_end_date = app_config["Datetime_hotstart"]
    kw = {
        "path": app_config["path"],
        "start": model_start_date,
        "end": model_end_date,
        "output_start": copy.deepcopy(model_start_date),
        "output_end": copy.deepcopy(model_end_date),
        "output": False,
        "load": True,
        "missing_value": True,
    }

    update_and_run(**kw)

    # ----------------!!!Assimilating data from observations!!!----------------#
    print("... Assimilating data from observations ...")
    nc_update_values_main(app_config)

    # -----------!!!copy hotstart file INTO the Hotstart folder!!!-------------#
    copyHotstartFile("To_HotstartDirectory", app_config)


def main(app_config):
    print("::::::::::::::::::::Up-To-date is finsished:::::::::::::::::::::::::::")

    # -------------------------------------------------------------------------#
    # STEP4:  Conducting the forecasting
    # -------------------------------------------------------------------------#

    print(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
    print("...............Producing forecast via Hotstart.....................")

    # -----------!!!copy hotstart file INTO the Model folder!!!-------------#
    copyHotstartFile("To_ModelDirectory", app_config)

    # Getting info about the hotstart file:
    dt_hotstart = get_info_from_netcdf_file(
        app_config["rootModel"], app_config["nc_restart"], app_config["obs_dict"]
    )
    dt_hotstart = datetime.strptime(dt_hotstart, "%Y-%m-%d %H:%M:%S")

    dt_diff = abs((app_config["Datetime_hotstart"] - dt_hotstart).seconds // 3600)

    if dt_diff > 5:
        print(
            "Be aware there is a differnce in datetimes between hotstart and target of %s"
            % dt_diff
        )

    # date configurations
    model_start_date = app_config["Datetime_hotstart"]
    # the model end date is set based relative to the Datetime_targetFmt explicit
    # and not the "Datetime_hotstart" because the later may be shifted back in time
    # if using profiles as controlling feature.

    model_end_date = app_config["Datetime_targetFmt"] + timedelta(days=dt_days_forecast)

    kw = {
        "path": app_config["path"],
        "start": model_start_date,
        "end": model_end_date,
        "output_start": copy.deepcopy(model_start_date),
        "output_end": copy.deepcopy(model_end_date),
        "output": True,
        "load": True,
        "missing_value": True,
    }
    update_and_run(**kw)


if __name__ == "__main__":
    if 1:
        use_restart(app_config)
    else:
        use_warmup(app_config)
    main(app_config)
