# -*- coding: utf-8 -*-
"""
Author:         Anders Nielsen
Description:    --.
Disclamer:      --.                

"""
import ruamel.yaml as yaml
import os
import os.path
import subprocess
import netCDF4
from datetime import datetime

"""
gotm_catalog_change = {
            "period/start"  : forecast_model_start_date,
            "period/stop"  : forecast_model_end_date
            }

fabm_catalog_change = {
            "instances/abiotic_water/parameters/hNO3Denit" : -999,
            }
"""
class gotm_yaml(object):

    def updateyaml(self, root,dict_catalog,yaml_file_name):
    
        yaml_file = os.path.join(root, yaml_file_name)
        yaml_file_out = os.path.join(root, yaml_file_name)
        # backup of the yaml file could be implemented
        # yaml_file_out = os.path.join(root, "out.yaml")
        
        with open(yaml_file, 'r') as f_in:
            yaml_in = yaml.round_trip_load(f_in)
    
        for variable, value in dict_catalog.items():
                
            yaml_k = list(variable.split('/'))
                                              

            self.updateInYaml(yaml_in,yaml_k,value)

        with open(yaml_file_out, "w") as f_out:
            yaml.round_trip_dump(yaml_in,f_out, default_style=None, default_flow_style=False)
    
    def updateInYaml(self, yaml_tree, yaml_key, value_update): 
        #print(yaml_key)
        if len(yaml_key) == 1: 
            yaml_tree[yaml_key[0]] = value_update
            return
        if not yaml_key[0] in yaml_tree:
            yaml_tree[yaml_key[0]] = {}
        self.updateInYaml(yaml_tree[yaml_key[0]], yaml_key[1:], value_update)
        return

def run_model(cmd ,root_model):
    #in_File = "gotm_release.exe" #  "run_gotm_release.bat"
                
    #File_Physical   = os.path.join(root_model, in_file)
		        
    run_model = subprocess.Popen(cmd.split() , cwd=root_model )
    run_model.wait()
    return 

def get_info_from_netcdf_file(root_model, nc_file, variables):
    restart_file = os.path.join(root_model, nc_file)
    # get creation day of the reset file:
    
    creation_date = datetime.fromtimestamp(os.path.getmtime(restart_file)).strftime('%Y-%m-%d %H:%M:%S') 
    print('The run will be based on a restart file from: %s' % creation_date)
    
    
    with netCDF4.Dataset(restart_file) as nc:
        #variable = "temp"
        nctime = nc.variables['time']
        t_centers = nctime[:]
        time_unit = nctime.units
        
        restart_time = (netCDF4.num2date(t_centers, time_unit))[0].strftime('%Y-%m-%d %H:%M:%S')     
        
        print('the date of initial conditions in the restart.nc is: %s' % restart_time)
        
        for variable in variables:
            var_data = nc.variables[variable][..., 0, 0]
            
            print('the %s initial condition will be set to:' %variable)
            print('min: %s' % var_data.min())
            print('mean: %s' % var_data.mean())
            print('max: %s' % var_data.max())
            print("...")
    
    return restart_time

