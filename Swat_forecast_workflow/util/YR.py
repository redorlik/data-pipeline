import requests as r
import datetime 
from collections import defaultdict
import math
from .radiation import shortwave_radiation

url = "https://api.met.no/weatherapi/locationforecast/2.0/complete"
        #Example url:
        #url = "https://api.met.no/weatherapi/locationforecast/2.0/compact?lat=60.10&lon=9.58"
        
        # request parameters for lake almind:

headers             = {"User-Agent": "My User Agent 1.0",
                               "From": "al@waterwebtools.com"
                              }

def get_forecast_YR(lat,lon,elevation):
    request_parameters  = {"altitude": elevation,
                              "lat":lat,
                              "lon":lon
                              }
    if lon>180:
        lon = (lon-360)
    resp = r.get(url, headers=headers, params=request_parameters)
    data = resp.json()
    time_series = data["properties"]["timeseries"]
    hh = time_series[0]['time'].split('T')[1].split(':')[0]
    dd = datetime.datetime.strptime(time_series[0]['time'],'%Y-%m-%dT%H:%M:%SZ')
    dd_t = dd.timetuple()
    hh = dd_t.tm_hour
    yday = dd_t.tm_yday
    update_yr = data['properties']['meta']['updated_at']
    time_series = data["properties"]["timeseries"]
    res = defaultdict(list)
    for x in time_series:
        res['time'].append(x['time'])
        res['t2m'].append(x['data']['instant']['details']['air_temperature'])
        if x['data'].get('next_6_hours'):
            res['t2m_min'].append(x['data']['next_6_hours']['details']['air_temperature_min'])
            res['t2m_max'].append(x['data']['next_6_hours']['details']['air_temperature_max'])
        else:
            res['t2m_min'].append(res['t2m'][-1])
            res['t2m_max'].append(res['t2m'][-1])
        res['msp'].append(x['data']['instant']['details']['air_pressure_at_sea_level'])
        res['tcc'].append(x['data']['instant']['details']['cloud_area_fraction']/100)
        res['d2m'].append(x['data']['instant']['details']['dew_point_temperature'])
        res['relhum'].append(x['data']['instant']['details']['relative_humidity'])
        theta = (270 - x['data']['instant']['details']['wind_from_direction'])/180*math.pi
        ws = x['data']['instant']['details']['wind_speed']
        res['u10'].append(ws*math.cos(theta))
        res['v10'].append(ws*math.sin(theta))
        if x['data'].get('next_1_hours'):
            res['tp'].append(x['data']['next_1_hours']['details']['precipitation_amount'])
        elif x['data'].get('next_6_hours'):
            res['tp'].append(x['data']['next_6_hours']['details']['precipitation_amount'])
        else:
            res['tp'].append(0.0000001)
        
        res['ssrd'].append(shortwave_radiation(hh, yday,lat,lon,x['data']['instant']['details']['cloud_area_fraction']/100))
    return res,update_yr

def update_YR_table(lat,lon,id):
    res,update_yr = get_forecast_YR(lat,lon,30)
    keys = ','.join([k for k in res.keys()])+',loc,provenance,updated'
    vals = ','.join([str(tuple((lat,lon)+v+(id,'YR',datetime.datetime.now().isoformat()))) 
            for v in zip(*res.values())])
    print(keys,vals)
    return keys,vals
    #query2 = f"insert into gfs2 (lat,lon,loc,{keys}) values {id},{vals}"


if __name__=='__main__':
    update_YR_table(56.15,9.54,39)