import requests as r
from .sms import send_message,send_email
from datetime import datetime,timedelta
from .post_vars import test2
import attr
import platform 

base_uri = 'asap-forecast.com'
gql_uri = 'http://test.asap-forecast.com:8081/graphql'
mobile_uri = 'http://places.asap-forecast.com:2000/places'
test_host = 'test.asap-forecast.com'
rural_projects = ['kaiping','vejle','richland-chambers']
reserv_projects = ['almind','ravn']
live_projects = ['almind']
forecasts = [('Yr',8),('Gfs',14)]

def reservoir_monitor(project, host=base_uri):
    res_uri = f'https://{host}/asap/reservoir/data/{project}'
    resp = r.post(res_uri,json=test2)
    if resp.status_code == 200 and resp.json():
        ret = {'ok':'Ok: Reservoir'}
    else:
        ret = {'error':f'No or wrong connection to reservoir server for {project}'}
    return ret

def live_monitor(project, host=base_uri):
    live_q="select DATEDIFF(SECOND,'1970-01-01',TimeStamp), value,'0.573',sensorid from AlmindsoeData where timestamp>='{start}'\
                and timestamp<'{end}' order by timestamp asc;"

    end = datetime.now()#"2020-12-17"
    start = end-timedelta(1)#"2020-12-27"
    live_uri = f'http://35.156.162.206:5000/sql?query={live_q.format(start=start.date(),end=end.date())}'
    resp = r.get(live_uri)
    if resp.status_code == 200:
        ret = {'ok':'Ok: Live'}
    else:
        ret = {'error':f'No or wrong connection to live server for {project}'}
    return ret

def rural_monitor(project, host=base_uri):
    
    url = f'https://{host}/asap/rural/data/{project}'
    res = r.get(url)
    delta = datetime.strptime(res.json(),"%Y-%m-%d %H:%M:%S")-datetime.now()
    if delta<=8*timedelta(1):
        ret = {'error':f'Forecast too old ({delta})for {project}'}
    else:
        ret = {'ok':'Ok'}
    return ret

def yr_monitor(service, host_uri=gql_uri):
    gql = '''{{forecastMulti(service:{service},place:almind,start:"{start}",
        end:"{end}"){{
        data{{
            time
        }}
            }}}}'''
    now = datetime.now()
    plus_m = now+timedelta(30)
    delta = service[1]
    srvce = service[0]
    js = gql.format(service=srvce,start=now.isoformat(),end=plus_m.isoformat())
    res = r.post(host_uri,json={'query':js})
    data = res.json()
    max_date = data['data']['forecastMulti']['data'][-1]['time']
    max_date = datetime.strptime(max_date,'%Y-%m-%dT%H:%M:%S')
    if max_date-now<=delta*timedelta(1):
        ret = {'error':f'Forecast too old ({delta})for service: {srvce}'}
    else:
        ret = {'ok':'Ok'}
    return ret

projects = {
        rural_monitor:rural_projects,
        reservoir_monitor:reserv_projects,
        #live_monitor:live_projects,
        yr_monitor: forecasts
           }

def error(k,msg):
    print('Error',k,msg)
    mess = f"ASAP error:{k} = {msg}"
    #send_email(mess)
    return (mess)

def success(k,msg):
    print(msg)

def monitor():
    node = platform.node()
    errors = []
    for func,projs in projects.items():
        for project in projs:
            try:
                ret = func(project)
            except Exception as e:
                ret['error'] = f"{project} has error: {e}"
            if 'error' in ret.keys():
                errors.append(ret['error'])
            
                
    if errors :
        send_email(node+': '+'\n'.join(errors))
        #send_message('Errors: check your email')
    else:
        pass
        #send_email(f"{node}: No errors")

if __name__ == '__main__':
    monitor()
