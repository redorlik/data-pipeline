import os,shutil,sys

def cp(root,dest):
    for d in os.listdir(root):
        archive_path = os.path.join(root,d)
        dest_path = os.path.join(dest,d)
        if d == 'archive':
            try:
                shutil.copytree(archive_path,dest_path)
            except FileExistsError as e:
                print(e)
        elif d in ['rural','reservoir']:
            cp(archive_path,dest_path)

if __name__=='__main__':
    root = sys.argv[1]
    dest = sys.argv[2]
    for p in os.listdir(root):
        r = os.path.join(root,p)
        d = os.path.join(dest,p)
        if not p.startswith('.'): cp(r,d)  