from siphon.catalog import TDSCatalog
from netCDF4 import num2date
from datetime import datetime,timedelta
from numpy import datetime_as_string,datetime64 as np_date,ma
import psycopg2 as pg
import sys
from .YR import update_YR_table
import click
from .Climate_data import PG

best_gfs = TDSCatalog('http://thredds.ucar.edu/thredds/catalog/grib/NCEP/GFS/'
                      'Global_0p25deg/catalog.xml?dataset=grib/NCEP/GFS/Global_0p25deg/Best')
best_ds = best_gfs.datasets[0]
ncss = best_ds.subset()
update_YR_table.table='yr'

create_table = """
DROP TABLE if exists public.GFS2 CASCADE;
CREATE TABLE public.GFS2 (
    latitude double precision default 56.4 ,
    longitude double precision default 9.4,
    time timestamp without time zone,
    u10 real,
    v10 real,
    relhum real default 14,
    t2m real,
    t2m_min real,
    t2m_max real,
    msp real,
    tcc real,
    tp real,
    ssrd real,
    provenance text DEFAULT 'GFS',
    updated timestamp ,
    location integer,
    CONSTRAINT fk_gfs2_location FOREIGN KEY (location)
        REFERENCES public.location (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
    );¨


DROP INDEX if exists idx_place_time;
    CREATE UNIQUE INDEX idx_place_time
    ON public.gfs2 USING btree
    (latitude ASC NULLS LAST, longitude ASC NULLS LAST, "time" ASC NULLS LAST)
    TABLESPACE pg_default;"""
windows_host = 'localhost' #'10.42.5.110'
windows_host_str = "postgres://ale@localhost:5432/CDS"
localhost ="postgres://postgres@localhost:5432/CDS"


def initDb():
    con = pg.connect(windows_host)
    con.autocommit = True
    with con.cursor() as cur:
        cur.execute(create_table)
    con.close()

def get_weather_GFS(lat,lon,forecast_days):
    var_0m = ['Temperature_height_above_ground',                                                   # Kelvin
                'Relative_humidity_height_above_ground',
                'Dewpoint_temperature_height_above_ground',
                'Minimum_temperature_height_above_ground_Mixed_intervals_Minimum',                # Kelvin
                'Maximum_temperature_height_above_ground_Mixed_intervals_Maximum',
                ]

    var_2m = ['Temperature_height_above_ground',                                                   # Kelvin
                                # Kelvin
                ]
    var_10m = ['u-component_of_wind_height_above_ground',                                         # m/sec
                'v-component_of_wind_height_above_ground']
    var_iso = ['Total_precipitation_surface_Mixed_intervals_Accumulation',                   # kg/m2 ~ mm
                'Downward_Short-Wave_Radiation_Flux_surface_Mixed_intervals_Average',             # W/m2 - the incoming swr is needed for SWAT
                'Downward_Long-Wave_Radp_Flux_surface_Mixed_intervals_Average',                   # W/m2
                'Pressure_surface',                                                               # Pa
                'Total_cloud_cover_entire_atmosphere_Mixed_intervals_Average']
    query = ncss.query()
    now = datetime.utcnow()
    _ = (query.lonlat_point(lon, lat).
            vertical_level(0.0).
            time_range(now, now + timedelta(days=forecast_days))) # get data at 2 m height above ground
    query.variables(*var_0m).accept('netcdf')                         # percent (0-100)
    data_0m = ncss.get_data(query)

    _ = (query.lonlat_point(lon, lat).
            vertical_level(2.0).
            time_range(now, now + timedelta(days=forecast_days))) # get data at 2 m height above ground
    query.variables(*var_2m).accept('netcdf')                         # percent (0-100)
    data_2m = ncss.get_data(query)

    _ = (query.lonlat_point(lon, lat).
        vertical_level(10.0).
        time_range(now, now + timedelta(days=forecast_days))) # get data at 10 m height above ground
    query.variables(*var_10m).accept('netcdf')                       # m/sec
    data_10m = ncss.get_data(query)

    _ = (query.lonlat_point(lon, lat).
        vertical_level(100000).
        time_range(now, now + timedelta(days=forecast_days))) # get data at 100000 Pa, approx. surface
    query.variables(*var_iso).accept('netcdf')   # percent (0-100)
    data_iso = ncss.get_data(query)
    time_ = data_2m.variables['time']

    #test = xarray.open_dataset(xarray.backends.NetCDF4DataStore(data_iso))
    res = dict(
       # latitude = lat,
       # longitude = lon,
        time = num2date(time_[:],time_.units,),
        t2m = data_2m.variables['Temperature_height_above_ground'][:]-273.15,
        t2m_min = data_0m.variables['Minimum_temperature_height_above_ground_Mixed_intervals_Minimum'],
        t2m_max = data_0m.variables['Maximum_temperature_height_above_ground_Mixed_intervals_Maximum'],
        d2m = data_0m.variables['Dewpoint_temperature_height_above_ground'][:]-273.15,
        relhum = data_0m.variables['Relative_humidity_height_above_ground'][:]/100,
        u10 = data_10m.variables['u-component_of_wind_height_above_ground'],
        v10 = data_10m.variables['v-component_of_wind_height_above_ground'],
        tp = data_iso.variables['Total_precipitation_surface_Mixed_intervals_Accumulation'],
        ssrd = data_iso.variables['Downward_Short-Wave_Radiation_Flux_surface_Mixed_intervals_Average'],
        #lwr = data_iso.variables['Downward_Long-Wave_Radp_Flux_surface_Mixed_intervals_Average'],
        msp = data_iso.variables['Pressure_surface'],
        tcc = data_iso.variables['Total_cloud_cover_entire_atmosphere_Mixed_intervals_Average'])
    #data_2m.close()
    #data_10m.close()
    #data_iso.close()

    return res

def update_gfs_table(lat,lon,id):

    x = get_weather_GFS(lat,lon,16)
    x['time'] = datetime_as_string(x['time'].astype('datetime64[s]'))
    print(x['time'])
    m=[y[:].squeeze() for y in x.values()]
    p=ma.stack(m,axis=1)
    now = datetime.utcnow()
    pp = [(lat,lon)+tuple(x)+(id,'GFS',str(now)) for x in p]
    cols = ', '.join([f'{t}' for t in x.keys()]+['loc','provenance','updated'])
    vals = ', '.join([f'{tt}' for tt in pp if 'nan' not in tt])
    #print(cols)
    #print(vals)
    return cols,vals
update_gfs_table.table='gfs'
@click.command()
@click.option("--host",required=True,default='postgres://postgres:rebus@localhost:5432/CDS',help='db host string')
@click.option('--port',required=True,default=5432,help='Folder apth for SWAT input files')
@click.option('--dbuser',required=False,default='ec2-user',help='user for remote server')
@click.option('--pw',required=False,default='',help='Folder apth for SWAT input files')
@click.option('--db',required=False,default='',help='user for remote server')
@click.option("--init",required=True,default=False,help='Init db')
def main(host,port,dbuser,pw,db,init):
    query = "INSERT into {table}2 (latitude,longitude,{cols}) values {vals} \
    on conflict (latitude,longitude,provenance,time) do update set ({cols})=\
     (excluded.time,excluded.t2m,excluded.t2m_min,excluded.t2m_max,\
         excluded.d2m, excluded.relhum, excluded.u10, excluded.v10,\
    excluded.tp,excluded.ssrd,excluded.msp, excluded.tcc,\
    excluded.loc,excluded.provenance,excluded.updated);"
 # time, t2m, t2m_min, t2m_max, d2m, relhum, u10, v10, tp, ssrd, msp, tcc, loc, provenance, updated
 # time,t2m,t2m_min,t2m_max,msp,tcc,d2m,relhum,u10,v10,tp,loc,provenance,update
    query2 = "INSERT into {table}3 (latitude,longitude,{cols}) values {vals};"
    query3 = """delete from yr4 *; insert into yr4 
    (latitude,longitude,time,t2m,t2m_min,t2m_max,msp,
    tcc,d2m,relhum,u10,v10,tp,ssrd,loc,provenance,updated)
    select distinct on (time,latitude,longitude) 
    latitude,longitude,time,t2m,t2m_min,t2m_max,msp,tcc,
    d2m,relhum,u10,v10,tp,ssrd,loc,provenance,updated
    from (select * from yr3 where time >'{start}') r 
    order by time desc,latitude,longitude ,updated desc; 
			"""
    # seest: lat:55.490070, lng:9.40301
    # kaiping lat:22.523669,lng: 112.422059
    if  0 : #init:
        initDb()
    pg = PG(host=host,port=port,user=dbuser,pw=pw,db=db)
    print(pg.pghost)

    active_places = pg.get_active_places()
    start = datetime.now()-timedelta(14)
    print (active_places)
    for p,lat,lon,id in active_places:
        for func in [update_YR_table]:#[update_gfs_table, update_YR_table]:
            cols,vals = func(lat,lon,id)

            que = query.format(table='yr',cols=cols,vals=vals)
            que2 = query2.format(table='yr',cols=cols,vals=vals)
            que3 = query3.format(start=start.date().isoformat())
            if 1: #func == update_YR_table:
                print(que)
                print(que3)

            pg.execute(que)
            #print(que2)
            pg.execute(que2)
            pg.execute(que3)

if __name__ == '__main__':
    main()

