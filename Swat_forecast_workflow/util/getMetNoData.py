# -*- coding: utf-8 -*-

"""
Script to request weather forecast data from MET.NO. they have a service that 
delivers a full weather forecast for one location, that is, a forecast with 
several parameters for a nine-day period.

Important from MET.NO:

Most important rules:

    You must identify yourself
    You must not cause unnecessary traffic
    You must not overload our servers

All requests must (if possible) include an identifying User Agent-string (UA) 
in the request with the application/domain name, optionally version number. 
You should also include a company email address or a link to the company 
website where we can find contact information. If we cannot contact you in 
case of problems, you risk being blocked without warning.

Own notes to the above:
We should include the following at our webpage: «Data from MET Norway»

Links:
from the old documentation:
https://api.met.no/weatherapi/locationforecast/1.9/documentation

Examples via python:
https://stackoverflow.com/questions/10606133/sending-user-agent-using-requests-library-in-python
https://api.met.no/weatherapi/locationforecast/2.0/documentation#/

getting threds of forecasts in netCDF:
https://thredds.met.no/thredds/metno.html
"""

import requests

import os.path
import json
import datetime
#import numpy as np
import math

class fetch_yr(object):
    
    def run(self):
        
        current_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        
        print("script has been run at:")
        print(current_time)
        

        forecastsPathOut = "U:/Anders/scripts/YR_get_data/individualForecasts"
        url = "https://api.met.no/weatherapi/locationforecast/2.0/complete.json"
        #Example url:
        #url = "https://api.met.no/weatherapi/locationforecast/2.0/compact?lat=60.10&lon=9.58"
        
        # request parameters for lake almind:
        request_parameters  = {"altitude": "30",
                              "lat":"56.1484",
                              "lon":"9.5457"
                              }
        
        headers             = {"User-Agent": "My User Agent 1.0",
                               "From": "youremail@domain.com"
                              }
        
        
        # executing the request:
        response = requests.get(url, headers=headers, params=request_parameters)
        
        # getting status:
        status  = response.status_code
        if status == 200:
            print("forecast status is ok")
            flag = "Y"
        elif status == 403:
            print("User-Agent forbidden error")
            flag = "N"
        
        
        if flag == "Y":
            data = response.json()
            
         
        UpdateAt = data["properties"]["meta"]["updated_at"]
        print(UpdateAt)
        
        get_timeseries = data["properties"]["timeseries"]
        
        jsonData = {}
        for timeseries_i in get_timeseries:
            timestamp = timeseries_i["time"]
            timestamp
            timeseries_i_data = timeseries_i["data"]["instant"]["details"]
            
               
            cloudiness = round(float(timeseries_i_data['cloud_area_fraction'])/100,3) # changing units. tcc
            pressure = float(timeseries_i_data['air_pressure_at_sea_level']) #msp
            temperature = float(timeseries_i_data['air_temperature']) #t2m
            dewpoint = float(timeseries_i_data['dew_point_temperature']) #d2m
            windDirection = float(timeseries_i_data['wind_from_direction'])
            windSpeed = float(timeseries_i_data['wind_speed'])
            relhum = float(timeseries_i_data['relative_humidity'])
            tp = timeseries_i['data']['1_hour']['details']['precipitation_amount']
            # calculating wind components.
            u10_wind = round(windSpeed*math.cos(270-windDirection),3) #u10
            v10_wind = round(windSpeed*math.sin(270-windDirection),3) #v10      
            
            jsonData[timestamp] = {"u10_wind" : u10_wind,
                                           "v10_wind" : v10_wind,
                                           "pressure" : pressure,
                                           "temperature" : temperature,   
                                           "dewpoint" : dewpoint,                     
                                           "cloudiness" : cloudiness,
                                           "relhum" : relhum
                                           #ssrd
                                           #tp
                                           }
        
        
        # export
        forecastOut = os.path.normpath(os.path.join(forecastsPathOut,UpdateAt.split(":")[0]+".json"))
        
        if os.path.isfile(forecastOut):
            # file exists
            # skip out
            print("forecast already exist")
        else:
            print("forecast is exported")
            # export to json:
            with open(forecastOut, 'w') as outfile:
                json.dump(jsonData, outfile, indent = 4, ensure_ascii = False)    


        print("... ...")
                
        return

#__init__()

