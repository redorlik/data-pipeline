

from subprocess import Popen



for year in range(2022,2023):
    cmd = f"python Swat_forecast_workflow\\util\\pivot.py {year}"
    print(cmd)
    p = Popen(cmd.split(),shell=True)
    p.wait()
    print(f"Year {year} finished!")