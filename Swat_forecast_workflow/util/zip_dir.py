
import zipfile,os
from datetime import datetime as dt

def zip_dir(directory,zip_name,zip_path=None):
    
    zipdir = os.path.join(directory,'archive')
    if zip_path:
        zipdir =  zip_path
    print(zipdir)
    try:
        os.listdir(zipdir)
    except FileNotFoundError as e:
        print('Exception',e)
        os.makedirs(zipdir)
    archive = os.path.join(zipdir,zip_name)
    print('archive path',archive)
    with zipfile.ZipFile(archive,'w',zipfile.ZIP_DEFLATED) as zipf:        
        for f in os.listdir(directory):
            file_path = os.path.join(directory,f)
            if os.path.isfile(file_path):
                zipf.write(file_path,arcname=f)

if __name__ == '__main__':
    zip_dir('/Users/au261214/projects/Swat_app/scripts/build/ravn/',f'{dt.now()}.zip',
       '/Users/au261214/projects/test/archive')
