from ..Climate_data import PG
from datetime import datetime,timedelta
from ...pipeline.util import read_data

def test_tunnel():
    with PG(port=5434,db='kaiping',remote_host='asap-forecast.com',pw='eerrymypfido') as pg:
        res = pg.get_query("select max(index) from lsu")
        diff = res[0][0] - datetime.now()
        print(diff)
        assert diff>timedelta(14)

def test_Erken():
    with PG(port=5434,db='swat',schema='Erken',remote_host='test.asap-forecast.com',pw='Swat.cat5') as pg:
        read_data(pg,'Erken')
