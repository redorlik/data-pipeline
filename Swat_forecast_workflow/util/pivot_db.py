class Db_pivot(Pivot):
    db_str : str ='postgres://postgres@localhost:5432/ERA_pivot'

    def __attrs_post_init__(self):
        self.db_connect()
        self.latitude_fk = self.get_fk('latitude')
        self.longitude_fk = self.get_fk('longitude')
        self.time_fk = self.get_fk('time')
        self.data_read()

    def db_connect(self):
        self.conn = pg.connect(self.db_str)

    def db_close(self):
        self.conn.close()

    def get_fk(self,table):
        cur = self.conn.cursor()
        cur.execute(f'select * from {table};')
        dat = cur.fetchall()
        return {y:x for x,y in dat}

    def write(self,lon,lat,d):

        lat_i = self.latitude_fk[lat]
        lon_i = self.longitude_fk[lon]
        #t_i = [time_fk[t] for t in dat_array.time]
        #v = dat_array.sel(longitude=lon,latitude=lat)

        vars = get_rows(d)
        time_i = lambda x:self.time_fk[datetime.utcfromtimestamp(int(x)/1e9)]
        vals_ = [f'({time_i(x[0])},{lat_i},{lon_i},{join(x[1:])})'
                    for x in vars]    
        ins = f'insert into timeseries (time,lat,lon,u10,v10,d2m,t2m,sp,tcc,tp,ssrd) values {join(vals_)}'
        self.execute(ins)
        d.close()
        
    def execute(self,q):
        cur = self.conn.cursor()
        cur.execute(q)
        self.conn.commit()
