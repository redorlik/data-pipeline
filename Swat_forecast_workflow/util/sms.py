import boto3
import os,sys
from datetime import datetime,timedelta
import requests as r
import hashlib
import hmac

host = "email.eu-central-1.amazonaws.com"
region = "eu-central-1"
service = "ses"


access_key = os.environ.get('AWS_ACCESS_KEY_ID') 
secret_key = os.environ.get('AWS_SECRET_ACCESS_KEY') 
if access_key is None or secret_key is None:
    print('No access key is available.')
    sys.exit()




# Create an SNS client
client = boto3.client(
    "sns",
    aws_access_key_id=access_key,
    aws_secret_access_key=secret_key,
    region_name="eu-central-1"
)
mailclient = boto3.client(
    "ses",
    aws_access_key_id=access_key,
    aws_secret_access_key=secret_key,
    region_name="eu-central-1"
)
def send_email(message,source='error@asap-forecast.com',
                dest=['redorlik@gmail.com'],subject='Error in ASAP-FORECAST'):
    mailclient.send_email(
        Source = source,
        Destination = {'ToAddresses':dest},
        Message = {'Body':{'Text':{'Data':message,'Charset':'utf-8'}},
        'Subject':{'Data':subject,'Charset':'utf-8'}}
        
    )
    print('Sent')
 
def send_message(mess,number='+4521298441'):
    # Send your sms message.
    print("SMS >",mess)
    resp = client.publish(
            PhoneNumber = number,
            Message = mess
            )
    print(resp['ResponseMetadata'],resp.keys())

def check_output_file(file):
    now = datetime.now()
    try:
        timestamp = os.path.getmtime(file)
    
        mod_date = datetime.fromtimestamp(timestamp)
        age = now-mod_date
        if age>timedelta(1)/24:
            send_message(f"Model result file {file.split('/')[-1]} is older than expected, now: {age}")
    except FileNotFoundError as e:
        print(e)
if __name__=='__main__':
    send_email('Fejl 40',dest=['anders@ece.au.dk'],subject='Test')
