import psycopg2 as pg
import pytest

host = 'localhost'
port = 5433
db = 'CDS'
user = 'postgres'
pw = ''

pghost = f'postgres://{user}:{pw}@{host}:{port}/{db}'
con = pg.connect(pghost)
cur = con.cursor()
res = cur.execute("select name,schema from location where active")

active = [x for x in cur.fetchall()]
#print(active)
@pytest.mark.parametrize("table,schema",active) #[('Huizhou lake', 'public'), ('Mar Menor', 'public'), ('Lake Erken', 'public'), ('Carioca', 'public'), ('Ormstrup sø', 'public'), ('almind', 'public'), ('kaiping', 'public'), ('ravn', 'public'), ('vejle', 'public'), ('holstebro', 'api'), ('holstebro vandkraftsø', 'api'), ('hilst', 'api')])
def test_doubles(table,schema):
    
    cur = con.cursor()
    no_doubles = f'select c,time from (select count(*) as c,time from {schema}."{table}" group by time) counts where c>1'
    cur.execute(no_doubles)
    res = cur.fetchall()
    assert not res

@pytest.mark.parametrize("table,schema",active) #[('Huizhou lake', 'public'), ('Mar Menor', 'public'), ('Lake Erken', 'public'), ('Carioca', 'public'), ('Ormstrup sø', 'public'), ('almind', 'public'), ('kaiping', 'public'), ('ravn', 'public'), ('vejle', 'public'), ('holstebro', 'api'), ('holstebro vandkraftsø', 'api'), ('hilst', 'api')])
def test_contiguous(table,schema):
    
    cur = con.cursor()
    holes = f'''select count(*),change from (select 
       time, 
       lead(time, 1) over (order by time asc) - time as change
from {schema}."{table}" ) c group by change'''
    cur.execute(holes)
    res = cur.fetchall()
    #print (res)
    assert len([x for x in res]) == 2