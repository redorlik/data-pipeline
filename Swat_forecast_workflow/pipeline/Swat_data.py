from pandas import DataFrame
import requests as r
from . base_data import Data
import os
from datetime import datetime,date,timedelta
from subprocess import Popen

header = '''{prefix}{file}: Relative humidity data - file written by ASAP {date}
nbyr     tstep       lat       lon      elev
{nbyear:>4} {tstep:>9}  {lat:>8.3f}  {lon:>8.3f}  {elevation:>8.3f}\n'''
    

def jday(date):
    tpl = date.timetuple()
    return tpl.tm_yday

class Swat(Data):
    product = 'swat'
    tstep = 0
    query = """select extract(year from time) y,
            extract(doy from time) d,min(time) dt,sum(tp) pcp,avg(sqrt(u10*u10+v10*v10)) wnd,
            max(t2m) maxT,min(t2m) minT, sum(ssrd)/1e+6 slr,
            avg(relhum) relhum,avg(t2m) tc from {self.pg.schema}."{self.placename}"
            where time>='{self.start}'
            and time<'{self.end}'
            group by d,y order by y,d"""

    query_forecast = """select y,d,dt,pcp,wnd,maxT,minT,slr,relhum,tc from (select extract(year from time) y,
            extract(doy from time) d,min(time) dt,count(time) as hours,sum(tp) pcp,
            avg(sqrt(u10*u10+v10*v10)) wnd,
            max(t2m) maxT,min(t2m) minT, sum(ssrd)/1e+3 slr,
            avg(relhum) relhum,avg(t2m) tc
            from gfs2 join location on location.id=loc 
            where name='{self.placename}' and time >last_era5_fullday+1 and 
            time <='{self.end}' and provenance='GFS'
            and loc=id group by d,y order by y asc,d asc ) as q
            where hours>=4"""

    columns =['y','d','dt','pcp','wnd','maxT','minT','slr','relhum','tc']

    container = 'swatplus'
    swatplus = 'docker run --name {self.container} \
                -v {self.path}:/data/TxtInOut redorlik/swatplus'

    swatplus_output = {
         #           'stations':'stations.json',
                    'hru_pw':'hru_pw_day.txt',
                    'hru':'hru_wb_day.txt',
                    'riv':'channel_sd_day.txt',
                    'lsu':'lsunit_wb_day.txt',
                    'aquifer':'aquifer_day.txt'}
    
    setup = {
        'tmp':{'file':'tmp.tem','cols':['maxT','minT'],
            'header':[header],
                    'format':['8.5F','8.5F'],
            }, #max og min Celsius
        'slr':{'file':'slr.slr','cols':['slr'],'format':['8.5F'],
            'header':[header]},#Daily total radiation MJ/m2
        'hmd':{'file':'hmd.hmd','cols':['relhum'],'format':['8.5F'],
            'header':[header]},#Daily average Relative humidity
        'wnd':{'file':'wnd.wnd','cols':['wnd'],'format':['8.5F'],
            'header':[header]},#Daily average m/s
        'pcp':{'file':'pcp.pcp','cols':['pcp'],'format':['8.5F'],
            'header':[header]# mm
        }
    }
    time_cols = '{y:>4}   {d:>4}   '
    remotepath = 'flask_app/swat_output/'

    def write_data(self):
        years = (self.dataend.year-self.datastart.year) + 1
        print("path to swat setup",self.path)
        print(self.data)
        for key,info in self.setup.items():
            fil = os.path.join(self.path,f"{self.prefix}{info['file']}")
            print("File to be written",fil)
            tstep = self.tstep
            self.write_file(fil,self.data,info,years,tstep)
    
    def write_file(self,fil,data,info,years,tstep):
        print(fil)
        with open(fil,'w') as f:
            for l in info['header']:

                f.write(l.format(lat=self.place[1],lon=self.place[2],
                    elevation=self.place[-1],file=info['file'],
                    date=datetime.now(),nbyear=years,tstep=tstep,prefix=self.prefix))

            for y,d,*x in data[['y','d',*info['cols']]].values:
                f.write(self.time_cols.format(y=int(y),d=int(d)))
                for d,t,format in zip(x,info['cols'],info['format']):
                    f.write(f'{d:>{format}}    ')
                f.write('\n')

    def add_cli(self):
        for ext in self.setup.keys():
            with open(os.path.join(self.path,f'{ext}.cli'),'a') as f:
                print(f"{ext}.cli: {ext} file names - file written by WaterWebTools {date.today()}",file=f)
                print("filenames",file=f)
                for fi in os.listdir(self.path):
                    if fi.split('.')[-1] == ext:
                        print(fi,file=f)

    def write_meta(self):
        #self.add_cli()
        filename =f'{self.path}/time.sim'
        startYear,endYear = self.start.year,self.end.year
        startDay,endDay = jday(self.start),jday(self.end)
        timedotsim = f"time.sim: written by ASAP v0.5 on {datetime.now()}\n\
            day_start  yrc_start   day_end   yrc_end      step\n\
            {startDay:8}{startYear:10}{endDay:8}{endYear:10}         0\
        "
        with open(filename,'w') as f:
            f.write(timedotsim)
        nyskip = self.output_start.year - startYear - 1
        if nyskip<0:
            nyskip = 0
        filename =f'{self.path}/print.prt'
        with open(filename,'r') as f:
            fil = f.read()
        with open(filename,'w') as f:
            for i,line in enumerate(fil.split('\n')):
                if i==2:
                    rest = line.split()[1:]
                    line = '     '.join([str(nyskip)]+rest)

                f.write(line+'\n')
            

    def run(self):
        cmd = self.swatplus.format(self=self)
        print(self.path,cmd)
        proc = Popen(cmd.split(),cwd=f'{self.path}')
        proc.wait()
        if proc.returncode:
            print("Wet run ended with an error")
            return

    def upload(self):
        procs = list()
        for f in self.swatplus_output.values():
            cmd = f"scp {self.path}/{f} {self.remoteuser}@{self.remote}:{self.remotepath}"
            procs.append(Popen(cmd.split()))
        for p in procs:
            p.wait()
        resp = r.get(f'http://{self.remote}:{self.remoteport}/asap/rural/data/{self.placename}')

class Swat_yr(Swat):
    query_forecast = """select y,d,dt,pcp,wnd,maxT,minT,slr,relhum,tc from 
            (select extract(year from time) y,
            extract(doy from time) d,min(time) dt,count(time) as hours,sum(tp) pcp,
            avg(sqrt(u10*u10+v10*v10)) wnd,
            max(t2m) maxT,min(t2m) minT, sum(ssrd)/1e+3 slr,
            avg(relhum)/100 relhum,avg(t2m) tc
            from yr4 join location on location.id=loc 
            where name='{self.placename}' and time >last_era5_fullday+1 and 
            time <='{self.end}' and provenance='YR'
            and loc=id group by d,y order by y asc,d asc ) as q
            where hours>=4"""

class Swat_hourly(Swat):
    product = 'swat'
    query_pcp = """select extract(year from time) y,
            extract(doy from time) jd,extract(month from time) m,
            extract(day from time) d,extract(hour from time) h,
            time as dt,
            tp pcp, sqrt(u10*u10+v10*v10) wnd,
            t2m maxT, t2m minT, ssrd/1e+6 slr,
            relhum relhum,t2m tc from {self.pg.schema}."{self.placename}"
            where time>='{self.start}'
            and time<'{self.end}'
            order by y,jd,h"""    
    
    def __attrs_post_init__(self):
        super().__attrs_post_init__()

    def write_loop_24(self,f,info,data):
        for y,jd,m,d,h,*x in \
            data[['y','jd','m','d','h',*info['cols']]].values:
            f.write(self.time_cols.format(y=int(y),jd=int(jd),m=int(m),
                d=int(d),h=int(h)))
            for d,t,format in zip(x,info['cols'],info['format']):
                f.write(f'{d:>{format}}    ')
            f.write('\n') 

    def write_data(self):
        # Write all files in daily format
        super().write_data()
        #write pcp file in 24 hour format
        years = (self.dataend.year-self.datastart.year) + 1
        tstep = 24
        #self.dbcolumns = ['y','jd','m','d','h','dt','pcp','wnd','maxT','minT','slr','relhum','tc']
        self.data = self.do_query(self.query_pcp.format(self=self))
        print("Columns",self.data.columns)
        fil = os.path.join(self.path,f"{self.prefix}pcp.pcp")
        info = self.setup['pcp']
        self.time_cols = '{y:>4} {jd:>4}     {m:>02}   {d:>02}   {h:>02}   '
        with open(fil,'w') as f:
            for l in info['header']:
                print(l)
                f.write(l.format(lat=self.place[1],lon=self.place[2],
                    elevation=self.place[-1],file=info['file'],
                    date=datetime.now(),nbyear=years,tstep=tstep,prefix=self.prefix))

            self.write_loop_24(f,info,self.data)   

class Swat_hourly_yr(Swat_hourly):
    query_db_gfs    = """select y,d,dt,pcp,wnd,maxT,minT,slr,relhum,tc from (select extract(year from time) y,
            extract(doy from time) d,min(time) dt,count(time) as hours,sum(tp) pcp,
            avg(sqrt(u10*u10+v10*v10)) wnd,
            max(t2m) maxT,min(t2m) minT, sum(ssrd)/1e+3 slr,
            avg(relhum)/100 relhum,avg(t2m) tc
            from yr2 join location on location.id=loc
            where name='{self.placename}' and time >last_era5_fullday+1 and provenance='YR'
            and loc=id group by d,y order by y asc,d asc ) as q
            where hours>=4"""