from ..restart_utils import *
from ..Wet_data import Wet
from ..pipeline import wet_hotstart
from datetime import datetime,timedelta

from py.test import fixture 

# Tests:
#   restart: uden assimillation, xml, gql
#   restart_date: fixed date, today,last_era5
#   warmup: antid, antal restarts, daily,weekly
#   update: 
#   assimilation: gem simulated,interpoleret


# Scenario 0
#   Normal Wet kørsel
# Scenario 1
#   Wet warmup til en dato restart fra denne dato (uden observationer)
# Scenario 2
#   Wet warmup til en dato, assimiler observationer, restart fra denne dato
# Scenario 3
#   Restart fra restartfil opdater restart fil 
# Scenario 4
#   Restart  fra restartfil til slut af meteo_dat
# ...



app_config ={
    "restart_date":["last_era5_day","today"][0], 
    "restart_period":2,
    "assimilation":{
        "xml":{
            "xml_config": "config_obs.xml",
            "obsvar": ["abiotic_water_sO2W", "temp"],
            "assimilation_approach": "closestProfile",  
            "assimilation_day_threshold": 20,  
            "assimilation_plot_data": True,
            "rootObs": "/Users/au261214/projects/Swat_app/hotstart_flexible_observation_assimilation/Models/Almind/Observations",
            "assimilationRootPlots": "/Users/au261214/projects/Swat_app/hotstart_flexible_observation_assimilation/Models/Almind/Plots"
        },
        "gql":{
            "nr_of_sensors":10,
            "qgl_endpoint":"https://iot.asap-forecast.com",
            "variables":{"EUI":"0018b24000004bb0","from":115,"to":1688},
            "query":"query test_almind_nexsens(\n            $EUI:String!,$from:UnixDate!,$to:UnixDate!)\n            \n            {\n            tempCableReadingsLatest(EUI:$EUI, latest:$from, filterError:$to ){\n              timestamps\n              errorCode\n              errorMessage\n              t0: temp(index: 1)\n              t1: temp(index: 2)\n              t2: temp(index: 3)\n              t3: temp(index: 4)\n              t4: temp(index: 5)\n              t5: temp(index: 6)\n              t6: temp(index: 7)\n              t7: temp(index: 8)\n              t8: temp(index: 9)\n              t9: temp(index: 10)\n              depths\n              lastRecord\n              nextRecord\n            }\n          }"
        }}
}
# placename = folder, 
#                 prefix = '',
#                 path = pth,
#                 start = dates.start,
#                 end = dates.end,
#                 output_start = dates.output_start,
#                 pg = model_pg

dates = Dates()
pth = "/Users/au261214/projects/Swat_app/scripts/build/almind/"

@fixture
def data():
    dat = wet_climate(Wet,"almind",
        pth,dates)
    return dat

def test_restart_date(data):
   
    app_config['restart_date'] = "2022-09-23T14:31:10"
    assert restart_date(app_config['restart_date'],data).isoformat() == "2022-09-23T14:31:10"
    app_config['restart_date'] = "last_era5_day"
    assert restart_date(app_config['restart_date'],data)-datetime.now().date() <= timedelta(8)
    app_config['restart_date'] = "today"
    assert restart_date(app_config['restart_date'],data)-datetime.now() <= timedelta(1)
    
def test_gql(data):
    res,z = obs_gql(app_config,restart_date(app_config['restart_date'],data))
    assert len(res['values']) == len(z)

def test_no_assimilation(data):
    ac = app_config.copy() 
    ac["assimilation"] = ""
    assert True

def test_hotstart_gql(data):
    ac = app_config.copy() 
    ac["assimilation"] = {'gql':app_config["assimilation"]['gql']}
    param = {"name":"temp","values":temp}
    result = assimilate(pth )
    assert len(result['old_vals']) == 41
