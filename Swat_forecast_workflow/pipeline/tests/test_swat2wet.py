from sqlalchemy.engine import create_engine
from Swat_forecast_workflow.pipeline.SWATplusToQWET import *
from Swat_forecast_workflow.util.Climate_data import PG
from Swat_forecast_workflow.pipeline.util import read_data,insert_data


db = {'port':5434,'db':'kaiping','pw':'Swat.cat5'}

def test_getdata():
    pg = PG(port=5434,db='kaiping',pw='Swat.cat5')
    dat = get_data_from_db(pg,['flo_out'],'2021-06-01','2021-08-01',6)
    assert len(dat) == 61

def test_run():
    get_wet_data_from_swat('2021-06-01','2021-08-01',**db)

def test_read_data():
    dbstring = 'postgresql://postgres:rebus@localhost:5432/'
    read_data(project='richland-chambers')

def test_insert_data():
    dbstring = 'postgresql://postgres:eerrymypfido@localhost:5434/richland-chambers'
    con = create_engine(dbstring)
    insert_data('build', 'richland-chambers', con)

    