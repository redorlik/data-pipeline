#!/usr/bin/env python3

# Get ERA5 data from sql db to swat input file format
#python -mSwat_forecast_workflow.pipeline.Climate2Swat job-loop 
#--host test.asap-forecast.com

from distutils import extension
from ..util.Climate_data import (PG, get_data_latlon_fast,NotOnLandException,
        get_date_ranges)

from ..util.sms import send_email
from ..util.Climate_data import get_weather_for_location,get_data_latlon
from datetime import datetime,timedelta
import click
import time
from click_datetime import Datetime
import requests as r
from collections import defaultdict 
from subprocess import Popen,DEVNULL,STDOUT
from .Swat_data import Swat, Swat_hourly
from .Wet_data import Wet
import os,inspect
import boto3
from botocore.exceptions import ClientError
import json,zipfile
from tempfile import mkdtemp

TEST = False
setup = {
    'tmp':{'file':'Tmp.tmp','cols':['maxT','minT'],
        'header':['Station  tmp_CDS\n',
                    "Lati        {lat:>5.1f}\n",
                    "Long        {lon:>5.1f}\n",
                    "Elev        35\n"],'format':['5.1F','5.1F'],
        }, #max og min Celsius
    'slr':{'file':'slr.slr','cols':['slr'],'format':['8.3F'],
        'header':['Station  slr_CDS\n']},#Daily total radiation MJ/m2
    'hmd':{'file':'hmd.hmd','cols':['relhum'],'format':['8.3F'],
        'header':['Station  hmd_CDS\n']},#Daily average Relative humidity
    'wnd':{'file':'wnd.wnd','cols':['wnd'],'format':['8.3F'],
        'header':['Station  wnd_CDS\n']},#Daily average m/s
    'pcp':{'file':'pcp1.pcp','cols':['pcp'],'format':['8.3F'],
        'header':['Station  pcp_CDS\n',
                    "Lati     {lat:>5.1f}\n",
                    "Long     {lon:>5.1f}\n",
                    "Elev     35\n"]},# mm

}
header = '''{prefix}{file}: Relative humidity data - file written by ASAP {date}
nbyr     tstep       lat       lon      elev
{nbyear:>4} {tstep:>9}  {lat:>8.3f}  {lon:>8.3f}  {elevation:>8.3f}\n'''
    
sleep = 2
products = {'wet':Wet,'swat':Swat,'swat_hourly':Swat_hourly,'swat2wet':Swat}
# for job in jobs:
#      create location
#      add job lokation to list
# getweatherfor location(list,...)
# for lokation in list:
#     produkt(lokation).getdata
# save_data_to_s3()

def create_newlocation(place,lat,lon,pg):
    last_era5_day = pg.get_fullday_from_db(place)
    if not last_era5_day:
        pg.create_location(place,lat,lon,schema='api')
    p = (place,lat,lon,0)
    return p

def get_model_data(tempdir,models,place,start,end,lat,lon,pg,**kw):
    elevation = kw['elevation']
    elevation = elevation if elevation else 135
    start_perf = time.perf_counter()
    mods = [x for x in models.keys() if models[x]]
    print(models,mods)
    for model in mods:
        print("Data for model",model)
        moddir = os.path.join(tempdir,model)
        os.makedirs(moddir,exist_ok=True)
        with products[model](
            placename = place,
            prefix = place.replace(' ','_'),
            path = moddir,
            start = start,
            end = end+timedelta(1)/24.*23.5,
            output_start = start,
            pg = pg ) as mod:
            mod.place = (place,lat,lon,0,elevation)
            print("get data")
            try:
                mod.get_data()
                print("got data")
                if 'swat' in model:
                    mod.add_cli()
            except FileNotFoundError as e:
                print("File not found",e)

    print(time.perf_counter()-start_perf,end-start)
    return tempdir
    
def save_data_to_s3(tempdir,id,job):

    s3 = boto3.client('s3')
    zip_name = "WWT_data"

    
    
    import shutil
    shutil.make_archive(zip_name,'zip',tempdir)
        
    bucket = 'asap.api'
    zipfile = f'{zip_name}.zip'
    print(zipfile)
    try:
        response = s3.upload_file(zipfile, bucket, f'{id}/{zipfile}',
                ExtraArgs={'ACL':'public-read'})
    except ClientError as e:
        print(e)

message = """
Dear User of WaterWebTools Data API.

The data you requested, with job Id {id} and latitudes longitudes:

 {latlngs}

It took {exe:0.1f} seconds to collect data for {places} locations for {days} days from {storage} storage.

You can download the data contained in a zipfile using the following URL:

{url}

The URL will be valid for 30 days.

Thank you for using the WaterWebTools Data API.
"""

@click.group()
def cli():
    pass

data_method = {
    'default': get_weather_for_location,
    'location': get_data_latlon,
    'location_fast': get_data_latlon_fast,
    'time': get_weather_for_location
}
@cli.command()
@click.option('--freq',default=2,help="How often should we check the job queue (seconds)")
#@click.option('--agg',default='hour',help="use files aggregate by hour or month (default hour) ")
@click.option('--host',required=True,default='localhost',help='Job server')
@click.option('--port',required=True,default=8090,help='Job server port')
@click.option('--dbhost',required=True,default='localhost',help='Job server')
@click.option('--dbport',required=True,default=5432,help='Job server port')
@click.option('--data_dir',required=True,default='F:\\Data\\ERA5\\CDS\\global\\',help='Directory with ERA5 files')
def job_loop(freq,host,port,dbhost,dbport,data_dir):
    pw = os.environ.get("DB_PASSWORD","")
    pg = PG(host=dbhost,port=dbport,pw=pw,schema='api')
    url = f'http://{host}:{port}/wwt/api/'
    Run = True
    while Run:
        try:
            jobs = r.get(url+'jobs/job')
            r.post(url+'meta',json=get_date_ranges())
        except r.exceptions.ConnectionError as e:
            print(datetime.now())
            print(e)
            continue
        if TEST:
            Run = False
            with open("test_location.json","r") as f:
                obj = json.load(f)
        else:
            obj = json.loads(jobs.text)
        if obj:
            #print(jobs.text)
            full_day = datetime(2020,1,1) if TEST else pg.get_fullday_from_db('kaiping')
            full_day = datetime(full_day.year,full_day.month,full_day.day)
            start_time = time.perf_counter()
            locs = []
            newjobs = []
            tempdir = mkdtemp()
            start = datetime.strptime(obj['start'],'%Y-%m-%d')
            end = datetime.strptime(obj['end'],'%Y-%m-%d')
            if end>full_day: 
                end = full_day   
            for i,job in enumerate(obj['stations']):
                p = (f'{obj["place"]}{i}',float(job['lat']),float(job['lng']),start,end)
                locs.append(p)
            dir = obj.get('data_dir',None)
            print("Data dir:", dir,"."  )
            data_dir_ = 'O:\\Tech_ERA5\\CDS\\' if (dir and dir=="cloud") else data_dir
            fetch_method = obj.get('data_method','default')
            print(fetch_method,"<<<<<<<<<<<<<<<<<<<<<")
            method = data_method[fetch_method]
            method = method if method else get_weather_for_location
            try:
                resdf,start,end = method(locs,start,end,data_dir_)
            except NotOnLandException as e:
                resdf,start,end = get_weather_for_location(locs,start,end,data_dir_)
            print(resdf)
            for i,(name,df) in enumerate(resdf.items()):
                pg.dataframe2sql(name,df,replace='replace')
                job = obj
                job['lat'] = obj['stations'][i]['lat']
                job['lon'] = obj['stations'][i]['lng']
                job['elevation'] = obj['stations'][i]['elevation']
                job['place'] = locs[i][0]
                job['pg'] = pg
                job['start'] = start
                job['end'] = end
                try:
                    job['tempdir'] = tempdir
                    print(job)
                    print(tempdir)
                    modeldir = get_model_data(**job)
                    #tempdirs.append(modeldir)
                except Exception as e:
                    print("Exception !!!!",e)
            save_data_to_s3(tempdir,obj['id'],job)
            obj['exe'] = time.perf_counter()-start_time
            obj['days'] = (job['end']-job['start']).days + 1
            obj['places'] = len(obj['stations'])
            obj['latlngs'] = '\n'.join([f'{x["lat"]}, {x["lng"]}' for x in obj['stations']])
            email = obj.get('email')
            storage = job.get("data_dir","")
            storage = storage if storage else 'USB'
            obj['storage'] = storage
            print(message.format(**obj))
            print(email)
            if email : send_email(message.format(**obj),source='api@asap-forecast.com',
                            dest=[email],subject="Your data is ready")
            url_done = f'http://{host}:{port}/wwt/api/processes/{obj.get("id")}'
            r.get(url_done) #,data=newjob)
            print('job time',time.perf_counter()-start_time)
        else:
            print('.',end='')
            time.sleep(freq)

@cli.command()
@click.option('--place',required=True,default='kaiping',help='Name of place')
@click.option('--start',required=True,default='2020-04-20',type=Datetime(format='%Y-%m-%d'),\
                help='Start date of simulation (YYYY-MM-DD). Limited by the range of the climate data')
@click.option('--output_start',required=True,default='2019-04-20',type=Datetime(format='%Y-%m-%d'),\
                help='Start date of simulation (YYYY-MM-DD). Limited by the range of the climate data')
@click.option('--end',required=True,default='2029-07-14',type=Datetime(format='%Y-%m-%d'),\
                help='End date of simulation (YYYY-MM-DD). Limited by the range of the forecast data')
@click.option('--prefix',required=True,default='dashahe',help='Prefix for SWAT input files')
@click.option('--path',required=True,default='.',help='Folder apth for SWAT input files')
@click.option('--host',required=True,default='localhost',help='DB server')
@click.option('--port',required=True,default=5432,help='Folder path for SWAT input files')
@click.option('--product',required=False,default='swat',help='[swat/wet] input files generation')
@click.option('--mode',required=False,default='forecast',help='[swat/wet] input files generation')
@click.option('--user',required=False,default='ec2-user',help='user for remote server')
#@click.option('--remote',required=False,default='18.185.174.73',help='Remote data server')
#@click.option('--remoteport',required=False,default=8080,help='Remote data server')
@click.option('--schema',required=False,default='public',help='Schema to place result into')
def one_location(place,start,end,output_start,path,prefix,host,port,product,
        mode,user,schema):
    pg = PG(host=host,port=port,user=user,db='CDS',schema=schema)
    if mode == 'forecast':
        now = datetime.now()
        start = now - timedelta(14*365)
        end = now + timedelta(16)
        output_start = now - timedelta(365)
    print("start",path,place,start, end)
    data = products[product]
    query = f"select name,lat,lon,id from location where name='{place}' and {product}"
    print(query)
    p = pg.get_query(query)
    
    if not p:
        print("Place is not currently available")
        return
    p=p[0]
    #path= f'{path}/{p[0]}'
    print("place",p[0])
    try:
        with data(p,prefix, path,start,end,output_start,pg) as location:
            #print("last full",location.last_era5_fullday)
            #location.get_data()
            location.run_data_pipeline()
            # location.run()
            # location.upload()
    except FileNotFoundError:
        print("File not found: ",f'{path}/{p[0]}')

@cli.command()
@click.option('--start',required=True,default='2020-04-20',type=Datetime(format='%Y-%m-%d'),\
                help='Start date of simulation (YYYY-MM-DD). Limited by the range of the climate data')
@click.option('--output_start',required=True,default='2019-04-20',type=Datetime(format='%Y-%m-%d'),\
                help='Start date of simulation (YYYY-MM-DD). Limited by the range of the climate data')
@click.option('--end',required=True,default='2029-07-14',type=Datetime(format='%Y-%m-%d'),\
                help='End date of simulation (YYYY-MM-DD). Limited by the range of the forecast data')
@click.option('--prefix',required=True,default='dashahe',help='Prefix for SWAT input files')
@click.option('--path',required=True,default='.',help='Folder apth for SWAT input files')
@click.option('--host',required=True,default='localhost',help='DB server')
@click.option('--port',required=True,default=5432,help='Folder apth for SWAT input files')
@click.option('--db',required=False,default='CDS',help='Database name to use')
@click.option('--mode',required=False,default='forecast',help='[swat/wet] input files generation')
@click.option('--user',required=False,default='ec2-user',help='user for remote server')
@click.option('--schema',required=False,default='public',help='Schema name')
#@click.option('--remoteport',required=False,default=8080,help='Remote data server')
#@click.option('--remotepath',required=False,default='flask_app/reservoir/ravn',help='Remote path')
def all(start,end,output_start,path,prefix,host,port,mode,user,schema):
    pg = PG(host=host,port=port,user=user,db='CDS',schema=schema)
    if mode == 'forecast':
        now = datetime.now()
        start = now - timedelta(10*365)
        end = now + timedelta(16)
        output_start = now - timedelta(365)
    print("start",path)
    for data in [Wet,Swat]: #Swat
        places = pg.get_active_places(product=data.__name__.lower())
        
        for p in places:
            print(p[0])
            try:
                with data(p,prefix, f'{path}/{p[0]}',start,end,output_start,pg) as location:
                    
                    location.run_data_pipeline()
                    #location.get_data()
                    #location.run()
                    #location.upload()
            except FileNotFoundError:
                print("File not found: ")
                continue

            

if __name__=='__main__':
    cli()    

    
    