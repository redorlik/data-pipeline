from . base_data import Data
import requests as r
from ruamel.yaml import YAML
from datetime import datetime,timedelta
import collections
from subprocess import Popen,STDOUT
import os

yaml = YAML(typ='rt')


def update(d, u):
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = update(d.get(k, {}), v)
        else:
            d[k] = v
    return d

class Wet(Data):
    container = 'wet'
    restart = {'output':True,'load':False,'allow_missing_variable':False}
    remotepath = 'flask_app/reservoir/'
    product = 'wet'
    wet2020 = "/usr/local/bin/docker run   \
            -v {self.path}:/data --name {self.container} redorlik/gotm-wet2020"

    query_forecast = "Select time, u10, v10, msp/100 msp, t2m t2m, \
       d2m d2m, tcc/100 tcc, ssrd from gfs2 join location on name='{self.placename}' where\
       loc=id and time>last_era5_fullday+1 and time <'{self.end}' and provenance='GFS'\
       order by time asc"
    
    query= "Select time, u10, v10, msp, t2m, d2m, tcc, ssrd/3600 ssrd    from {self.pg.schema}.\"{self.placename}\" \
        where\
        time<'{self.end}' and time >='{self.start}'"
    meteo_columns = "  u10   v10  msp   t2m     d2m tcc".split()
    ssrd = ['ssrd']
    columns = ['dt'] + meteo_columns + ssrd

    #ERA5 ssrd er accumuleret over en time J/m2
    #GFS ssrd er i W/m2
    #Yr beregnes til W/m2

    def patch_data(self):
        path = f'{self.path}/{self.prefix}meteo_file.dat'
        self.query_db_GFS()
        # pd.read_csv(path)
        # replace last part of df
        
    def write_data(self):
        #info = setup[type]
        #cols = "  u10   v10  msp   t2m     d2m tcc".split()
        path = f'{self.path}/{self.prefix}meteo_file.dat'
        self.data[self.meteo_columns].to_csv(path,header=False,sep='\t',float_format='%.2f')
        path = f'{self.path}/{self.prefix}slr.dat'
        self.data[self.ssrd].to_csv(path,header=False,sep='\t',float_format='%.2f')

    def handle_yaml(self, target):

        fil = f'{self.path}/gotm.yaml'
        with open(fil,'r') as f:
            yml = yaml.load(f.read())
        res = update(yml,target)
        
        with open(fil,'w') as f:
            yaml.dump(res,f)

        # Is this necessary ?
        with open(fil,'r') as f:
            strg = ''.join(f.read().split("'"))
        with open(fil,'w') as f:    
            f.write(strg)
        

    def set_flow_chem(self,method):
        fields = ['flow','abiotic_water_sNH4W','abiotic_water_sNO3W','abiotic_water_sNPOMW',
            'abiotic_water_sNDOMW','abiotic_water_sPO4W','abiotic_water_sPPOMW',
            'abiotic_water_sPDOMW'] #,'abiotic_water_sSiO2W']
        
        streams={'streams':{
            'inflow':{ }
            }
        }
        for item in fields:
            streams['streams']['inflow'][item]= {'method':method}
        print(streams)
        return streams            

    def write_meta(self):
        def dt_hour(dt):
            if type(dt) == str:
                dt = dt.split('.')[0]

                return dt.split(':')[0]+":00:00"
            return datetime(dt.year,dt.month,dt.day,dt.hour)

        print("write metafiles")
        #Used for combined SWAT and WET (maybe hotstart too)
        target ={
            'output':
                {'output':
                    {'time_start':dt_hour(self.output_start.isoformat()),
                    'time_stop':dt_hour(self.output_end.isoformat()),
                    'time_unit':'hour',
                    'use':self.restart['output']}
                },
            'time':
                {'start':dt_hour(self.start.isoformat()),
                'stop':dt_hour(self.end.isoformat())},
            'surface':
                {'meteo':
                    {'hum':
                        {'type':3}
                    }
                },
            'restart':{
                'load':self.restart['load'],
                'allow_missing_variable':self.restart['allow_missing_variable']
                }
            }
        chem = 2 if 'inflow_chem.dat' in os.listdir(self.path) else 0
        res = update(target,self.set_flow_chem(chem))
        self.handle_yaml(res)
    
    def run(self):
        cmd = self.wet2020.format(self=self)
        #log.msg(f"Run docker: {cmd}")
        with open('docker1.log','a') as f:
            p = Popen(cmd.split(),cwd=self.path,stdout=f,stderr=STDOUT)
            p.wait()
        if p.returncode:
            print(f"Wet run ended with an error: {p.returncode}")
           # log.msg(f"Wet run ended with an error: {p.returncode}")
            return

    def upload(self):
        p = Popen(f"/usr/bin/scp {self.path}/output.nc\
             {self.remoteuser}@{self.remote}:{self.remotepath}/{self.placename}/".split())
        p.wait()

class Wet_yr(Wet):
    query_forecast = "Select time, u10, v10, msp, t2m, \
       d2m, tcc from yr2 join location on name='{self.placename}' where\
       loc=id and time>last_era5_fullday+1 and time <'{self.end}' and provenance='YR'\
       order by time asc"
#   
#   restart date:
#        last observation
#        Last era5 day
#        today
#   assimilation:
#       yes (infers restart):
#            file observations (last observation)
#            realtime (gql)(we can choose restart date)
#       no:
#
#   forecast:
#       start:
#           from period start to restart date (including warmup)
#           from restart date
#       end:
#           to update restart +1,2,3 days (hours?)
#           to end of period (depending on forecast product)

class Wet_restart(Wet):
    restart = {'output':False,'load':False,'allow_missing_variable':False}

class Wet_update_restart(Wet):
    restart = {'output':False,'load':True,'allow_missing_variable':True}

class Wet_hotstart(Wet):
    restart = {'output':True,'load':True,'allow_missing_variable':True}

