# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 13:40:45 2021

@author: au302246
"""

"""
# In time.sim we extract the following info:
# yrc_start    : Beginning year of simulation (in the old SWAT it was: IYR)
# day_start    : Beginning julian day of simulation (in the old SWAT it was: IDAF)

# In print.prt we extract the following info:
# nyskip       : number of years to skip output printing/summarization (in the old SWAT it was: NYSKIP)

with open(fullPathTime) as fp:
    for i,line in enumerate(fp):
        if i == 2:
            swat_day_start = int(line.split()[0]) + 1 # we add 1 to compensate for the counter in SWAT starting at 0
            swat_yrc_start = int(line.split()[1])

with open(fullPathPrint) as fp:
    for i,line in enumerate(fp):
        if i == 2:
            swat_nyskip = int(line.split()[0])

#swat operates with julianday of the year which has to be converted into a date
#reference:
#http://www.science-emergence.com/Articles/MODIS-Convert-Julian-Date-to-MMDDYYYY-Date-format-using-python/ 
month = 1
while swat_day_start - calendar.monthrange(swat_yrc_start,month)[1] > 0 and month <= 12:
    swat_day_start = swat_day_start - calendar.monthrange(swat_yrc_start,month)[1]
    month = month + 1
    
swat_startdate_simulation = datetime.datetime(swat_yrc_start,month, swat_day_start, 12, 0, 0)

#computing the startyear of the simulation accounting for skipyears:    
swat_startedate_output = swat_startdate_simulation.replace(swat_startdate_simulation.year + swat_nyskip)

"""
def JulianDate_to_MMDDYYY(y,jd):
    month = 1
    day = 0
    while jd - calendar.monthrange(y,month)[1] > 0 and month <= 12:
        jd = jd - calendar.monthrange(y,month)[1]
        month = month + 1
    print (month,jd,y)

