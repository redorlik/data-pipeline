
from Swat_forecast_workflow.pipeline.util import read_data,wet2db
from Swat_forecast_workflow.util.database_setup import (frontend_test,
        local_swat,climate_anydesk,climate_local)
from Swat_forecast_workflow.pipeline.Swat_data import Swat_yr as Swat
from Swat_forecast_workflow.pipeline.Wet_data import (Wet,Wet_restart,
    Wet_update_restart,Wet_hotstart)
from subprocess import Popen,PIPE
import os
from os.path import join as p_join
from .restart_utils import (obs_gql,obs_xml,wet_climate,wet_run,
    assimilate,get_restart_period,restart_date,Dates)
import json
import click
from shutil import copy2,move,rmtree
from datetime import datetime,timedelta
from ..util.sms import check_output_file
from ..util.database import PG
from ..util.zip_dir import zip_dir
from Swat_forecast_workflow.pipeline.SWATplusToQWET import (
    get_wet_data_from_swat)

func = {'xml':obs_xml,'gql':obs_gql}

class Ssh:
    def __init__(self,host,user,path):
        self.host = host
        self.user = user
        self.path = path

test = Ssh('test.asap-forecast.com','ec2-user','flask_app/data')
water1 = Ssh('18.185.244.98','ec2-user','models')
water2 = Ssh('3.124.95.86','ec2-user','models')

data_pipeline = 'python -m {climate2swat}\
         one-location --mode forecast --host {host} --port {port} --path {path} --place {place} \
         --product {product} --prefix {prefix} --user=postgres'         

swatplus_output = {'kaiping':{
                      'files':{
                    'hru_pw':'hru_pw_day.txt',
                    'hru':'hru_wb_day.txt',
                    'riv':'channel_sd_day.txt',
                    'lsu':'lsunit_wb_day.txt',
                    'aquifer':'aquifer_day.txt'},
                       'stations':['kaiping']},
                    
                    'vejle':{
                       'files':{
         #           'hru_pw':'hru_pw_day.txt',
         #           'hru':'hru_wb_day.txt',
                    'riv':'channel_sd_day.txt',
                    'lsu':'lsunit_wb_day.txt',
                    'aquifer':'aquifer_day.txt'},
                        'stations':['vejle','vandel']},
                    }

def get_stations(path):
    f = 'pcp.cli'
    # Read the actual existing cli files from file.cio
    #
    # with open(p_join(path,'file.cio'),'r') as fn:
    #     files = fn.readline()
    #     files = fn.readline()
    
    with open(p_join(path,f),'r') as fn:
        rd = fn.read()
        print(rd)
        x = [y[:-7] for y in rd.split('\n') if y][2:]
    return x
            

def prepare_swat(pth,folder,dates,pg):
    
    statns = get_stations(pth) #loc_setup['stations']
    print("stations",statns)
    if folder in swatplus_output:
        loc_setup = swatplus_output[folder]
        
    else:
        loc_setup = {
            'files':{
                'hru_pw':'hru_pw_day.txt',
                'hru':'hru_wb_day.txt',
                'riv':'channel_sd_day.txt',
                'lsu':'lsunit_wb_day.txt',
                'aquifer':'aquifer_day.txt'}
        }
        
    for loc in  statns:
 
        place =  'kaiping' if  loc=='dashahe' else loc
        # dat_cmd = data_pipeline.format(path=pth,place=loc,product='swat',
        #     prefix=loc,host=pg.host,port=pg.port,climate2swat=climate2swat )
        # dat.append(dat_cmd)
        
        data = Swat(placename = place,
                prefix = loc,
                path = pth,
                start = dates.start,
                end = dates.end+timedelta(2),
                output_start = dates.output_start, 
                pg = pg)
        data.run_data_pipeline()
    return loc_setup

def swat(*args,**kw):
    swatplus = 'docker run --name swatplus --rm\
                -v {path}:/data/TxtInOut redorlik/swatplus:60.5'
    error = []
    model_pg = PG(**climate_anydesk)
    pth = kw['path']
    if 'swatplusflood.exe' in os.listdir(pth):
        swatplus = '{path}/swatplusflood.exe'
    folder = kw['folder']
    dates = kw['dates']
    print(folder,pth,dates)
    loc_setup = prepare_swat(pth,folder,dates,model_pg)
    
    cmd = swatplus.format(path=pth)
    for f in loc_setup['files'].values():
        err = check_output_file(f"{pth}/{f}",folder)
        if err : 
            error.append(err+" "+f)
    print("Command: >>",cmd)
    p = Popen(cmd.split(),cwd=pth) #,stdout=PIPE)
    p.wait() #mdls.append((p,pth,folder))
    # res = pg.get_query(f"select tablename from pg_catalog.pg_tables \
    #         where schemaname='{folder}' and tablename='model_run';")
    # last_full_day = model_pg.get_full_day() \
    #     if len(res)==1 else None
    # print('--------------------->',last_full_day)
    read_data(folder,pth,
        last_full_day=None)
    now = datetime.now().isoformat().split('.')[0]
    now = now.replace(':','_')
    zip_dir(pth,f'{now}.zip')
    return error

def wet(**kw):
    # Get data
    pth = kw['path']
    folder = kw['folder']
    dates = kw['dates']
    sshs = kw['ssh']
    
    data = wet_climate(Wet,folder,pth,dates)
    error = wet_run(folder,pth,sshs)
    wet2db(pth,folder)
    now = datetime.now().isoformat().split('.')[0]
    now = now.replace(':','_')
    zip_dir(pth,f'{now}.zip')

    return error
    
def wet_hotstart(**kw):
    
    # Get climatedata
    pth = kw['path']
    folder = kw['folder']
    dates = kw['dates']
    sshs = kw['ssh']
    with open(p_join(pth,'config.json'),'r') as f:
        app_config = json.load(f)
    r_path = p_join(pth,"restart_files")
    if not os.path.exists(r_path):
        os.mkdir(r_path)
    cls = get_restart_period(app_config,r_path,pth)

    
    errors = []
    data = wet_climate(cls,folder,pth,dates)
    # Run warmup
    data.end = restart_date(app_config["restart_date"],data)
    data.write_meta()
    errors.append(wet_run(folder,pth,[]))
    #copy restart.nc
    old_nc = p_join(r_path,"restart.nc")
    if os.path.exists(old_nc):
        time = int(datetime.now().timestamp())
        move(old_nc,p_join(r_path,f"restart_{time}.nc"))
    copy2(p_join(pth,"restart.nc"),r_path)
    # Assimilate observation
    assim = app_config.get('assimilation',None)
    if assim:
        param,z = func[assim['type']](assim['conf'],data.end,pth)    
        assimilate(pth,param)

    data = wet_climate(Wet_hotstart,folder,pth,dates)
    #set start date to restart date ?
    errors.append(wet_run(folder,pth,sshs))
    return errors

def rural(path,folder,dates,ssh):
    print("<<<<<<<",path)
    return swat(path=p_join(path,'rural'),folder=folder,dates=dates)

def swat2wet(path,folder,dates,ssh):
    # To simplify only allow the following setup
    #           ./     reservoir  rural
    # swat2wet  x 
    # swat                        x
    # wet                 x
    # alternativ :
    #           ./     some_folder (recursive)
    # swat2wet  x          x  overules default setting for SWAT/WET
    # swat      x          x  
    # wet       x          x
    with open(p_join(path,'swat2wet.json'),'r') as f:
        config = json.load(f)
    swat_path = config['swat_path']
    print("<<<<<<<",path)
    print("<<<<<<<",swat_path)
    wet_path = config['wet_path']
    print("<<<<<<<",wet_path)
    now = datetime.now()
    dates = Dates(output_start = now - timedelta(365*3))
    
    err = swat(path=f'{swat_path}',folder=folder,dates=dates)
    dates = Dates(start= dates.output_start,
                output_start=now - timedelta(1*30))
    get_wet_data_from_swat(config,dates.start,dates.end)
    err.extend(wet(path=f'{wet_path}',folder=folder,dates=dates,ssh=ssh))
    return err

def reservoir(path,folder,dates,ssh):
    print("Reservoir")
    return wet(path=p_join(path,'reservoir'),folder=folder,dates=dates,ssh=ssh)
    
Swat_dates = Dates()
now = datetime.now()
Swat2wet_dates_swat = Dates(output_start=now-timedelta(3*365))
Swat2wet_dates_wet = Dates(start= Swat2wet_dates_swat.output_start,
                output_start=now - timedelta(1*30))
Wet_dates = Dates(start=now-timedelta(3*365),
                    output_start=now-timedelta(1*30) )
Wet_test_dates = Dates(start=datetime(2016,1,1),
                        end=datetime(2016,8,21),
                    output_start=datetime(2016,8,11)-timedelta(1*30) )
@click.command()
@click.option('--path',required=True,default='.',help='Folder path for input files')
@click.option('--host',required=True,default='localhost',help='DB server')
@click.option('--port',required=True,default=5432,help='Folder apth for SWAT input files')
@click.option('--remoteuser',required=False,default='ec2-user',help='user for remote server')
@click.option('--remote',required=False,default='asap-forecast.com',help='Remote data server')
@click.option('--remoteport',required=False,default=8080,help='Remote data server')
@click.option('--remotepath',required=False,default='flask_app/data',help='Remote path')
@click.option('--start',required=False,default=datetime.now()- timedelta(1*365),
                help='Start simulation date')
@click.option('--end',required=False,default=datetime.now()+ timedelta(14),
                help='End simulation date')
@click.option('--output_start',required=False,default=datetime.now() - timedelta(365),
                help='Start output date')
def main(path,host,port,remoteuser,remote,remoteport,remotepath,start,end,output_start):
    import pathlib
    my_path = pathlib.Path(__file__).parent.resolve().parts[-2:]
    climate2swat = '.'.join(list(my_path)+['Climate2Swat'])
    docker_pipeline = 'docker run -it --rm -v "{path}":/data/ --name pipeline redorlik/asap-datapipeline \
        one-location --mode forecast --host {host} --port {port} --path "{path}" --place "{place}"\
         --product {product} --prefix "{prefix}"'
    
    #run models    mdls = []
    upload = []
    for folder in os.listdir(path):

        if folder != 'marmenor':
               continue
        if folder.startswith('_'):
            continue
        cmd = ''
        pth = p_join(path,folder)
        print ("Folder path ",pth,os.path.isdir(pth))
        if not os.path.isdir(pth) :
            continue       
        error = []
        print(product(pth))
       
        for prd in product(pth):
            # if not prd == reservoir:
            #       continue
            dates = Swat_dates if prd in [swat,rural] else Wet_dates
            error.extend(prd(path=pth,folder=folder,dates=dates,ssh=[water1]))#,water2]))

    # Wait for models to finish
    # for m,pth,_ in mdls:
    #     m.wait()
    # upload data to server
    print("Models finished",upload)
    #if error: send_message(",".join(error))
    
def product(folder):
    files = os.listdir(folder)
    product = []
    if 'swat2wet.json' in files:
        product.append(swat2wet)
        return product
    if 'file.cio' in files:
        product.append(swat)
    if 'rural' in files:
        product.append(rural)
    if 'gotm.yaml' in files:
        if "config.json" in files:
            product.append(wet_hotstart)
        else:
            product.append(wet)
    if 'reservoir' in files:
        product.append(reservoir)
    return product
        
if __name__ == '__main__':
    print("start")
    main()