# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 13:47:13 2021

@author: au302246
"""

# filter on selected subbasins:
SWATSubbasinsSelected = [1]

   
#"Method": "SWATplus/Rating Curve/Constant/Timeseries",
# "Timeseries": "path to inflow_chem.dat & inflow.dat" - kan vi ikke bare kopiere filerne til wet folderen?
config = {
                         1: {
                                       
                              "variables": {"sPPOMW_konc_mg_l": curve('constant',0.5),
                                           "sPDOMW_konc_mg_l": curve('constant',0.7),
                                           "inflow": curve('constant',2.3)
                                          }
                                       
                            },
                         40:{
                         "outflow": "constant/Timeseries"
                        }
}

