import attr
import os,time
import pandas as pd
import structlog

structlog.configure(processors=[structlog.processors.TimeStamper(fmt='iso'),structlog.processors.JSONRenderer()])
log = structlog.get_logger()

@attr.s
class Data:
    
    placename = attr.ib() 
    prefix = attr.ib()
    path = attr.ib()
    start = attr.ib()
    end = attr.ib()
    output_start = attr.ib() 
    pg = attr.ib() 
    
    era5 = ''
    gfs = ''

    # def set_pg(self,pg):
    #     self.pg = pg

    def __attrs_post_init__(self):
        query = f"select name,lat,lon,id from location where name='{self.placename}' and {self.product}"
        print("+++Init+++",query)
        try:
            self.place = self.pg.get_query(query)[0]
        except IndexError as e:
            print('????????????',e)
        self.output_end = self.end
        self.last_full_day = self.pg.get_full_day()
        
    def __enter__(self):
        #self.placename = self.place[0]
        print("last full day",self.last_full_day,)
        os.makedirs(self.path, exist_ok = True)
        return self

    def __exit__(self,type,value,tb):
        pass

    def query_db(self):
        query = self.query.format(self=self)
        self.era5 = self.do_query(query)

    def do_query(self,query):
        print("query_db",query)
        d = self.pg.get_query(query)
        if d:
            dataset = pd.DataFrame(d)
            dataset.columns = self.columns
            dataset = dataset.set_index('dt')
            return dataset

    def query_db_GFS(self):
        print("GFS",self.placename,self.start,self.end)
        query = self.query_forecast.format(self=self)
        print(query)
        d = self.pg.get_query(query)
        
        dataset = pd.DataFrame(d)
        dataset.columns = self.columns
        dataset = dataset.set_index('dt')
        
        self.gfs = dataset
    
    def merge_data(self):
        
        
        print(f"Full_day and start for {self.place[0]}:",self.last_full_day,self.start,self.path)
        if self.start.date() <= self.last_full_day:
            df = self.era5
            if self.end.date()>self.last_full_day:
                df = df.append(self.gfs)
        else:
            df = self.gfs
        self.data = df
        self.dataend = df.index.max()
        self.datastart = df.index.min()
        if self.end > self.dataend:
            self.end = self.dataend
        if self.start < self.datastart:
            self.start = self.datastart
        log.msg (f"Start:{self.start} Datastart: {self.datastart}")
        log.msg (f"Start:{self.end} Datastart: {self.dataend}")
     
    def run_data_pipeline(self):
        
        log.msg(f"Start {self.placename}")
        self.query_db()
        log.msg("GFS")
        if self.end.date()>self.last_full_day:
            self.query_db_GFS()
        log.msg("Merge")
        self.merge_data()
        log.msg("Write data")
        self.write_data()
        log.msg("Meta")
        self.write_meta()
    
    def get_data(self):
        self.query_db()
        if self.end.date()>self.last_full_day:
            self.query_db_GFS()
        self.merge_data()
        self.write_data()
        
    def write_data(self):
        raise NotImplementedError

    def write_meta(self):
        raise NotImplementedError

    def run(self):
        raise NotImplementedError

    def upload(self):
        self.upload()
        log.msg(f"Done {self.placename}")
