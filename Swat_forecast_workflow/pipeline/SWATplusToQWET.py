# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 08:35:21 2021

@author: Anders Nielsen
Version 0.1

Converting SWATplus simulation scenarios to QWET input file format.
Currently SWATplus simulations are aggregated across selected units(subbasins)
into daily concentrations and flows.
In case it should be used for SWATplus running in subdaily mode, the time and
the conversion of flow in m3/s into daily flows should be changed. 
"""

import os.path
import pandas as pd
import numpy as np
import json
#local import
#from .SelectedUnits import SWATSubbasinsSelected
from ..util.Climate_data import PG
from math import log

filePath = "C:/temp/SWATPLUS/Erken/SWATplus_Erken/Scenarios/Dia11/TxtInOut"
filePathOut = "C:/temp/SWATPLUS/Python/filesOut"
fileNameChannel = "channel_sd_day.txt"
fullPathChannel = os.path.join(filePath,fileNameChannel)

SWATplusLookup = {
    'meta':{"jday"    : "julianday",
        "mon"    : "month",
        "day"    : "day",
        "yr"    : "year",
        "unit"    : "unit",
        "gis_id"    : "gis_id",
        "name"    : "name",},

      'data':{
        "sedp_out"    : "organic N",
        "solp_out"    : "mineral N",
        "orgn_out"    : "organic particulate N",
        "no3_out"    : "Nitrate",
        "no2_out"    : "Nitrite",
        "nh3_out"    : "Ammonia",
        "flo_out"    : "flow"
      }}

def curve(config,dtime=0):
    curve = config['rating_curve']
    def constant(Q):
        return args[0]

    def rc_1(Q):
        res = curve['a0']
        if Q>0:
            res += curve['a1']*(log(Q)-curve['T_tilde'])
        return res

    def timeseries(Q):
        pass

    funcs = {'constant':constant,
            'rate_1':rc_1,
    }
    return funcs(type)

def read_swat(fullPathChannel,SWATSubbasinsSelected):
    # getting a list of headers to be applied while importing the actual data:
    headerRead = pd.read_csv(fullPathChannel, header=0, skiprows=1, skipinitialspace=True, delimiter=r"\s+", nrows=1)
    headerReadlst = list(headerRead)

    # overview of columns from the SWAT plus output to work with:

    headerReadlstSelect =[]
    for name in headerReadlst:
        if name in SWATplusLookup['meta']:
            headerReadlstSelect.append(name)
        if name in SWATplusLookup['data']:
            headerReadlstSelect.append(name)
    # import of data:
    SWATimport = pd.read_csv(fullPathChannel, header=None, names = headerReadlst, skiprows=3, skipinitialspace=True, delimiter=r"\s+")#, nrows=5000)
    # selecting only certain columns:
    SWATchannelData = SWATimport[headerReadlstSelect]
    # creating datatime format:
    SWATchannelData["datetime"] = pd.to_datetime((SWATchannelData["yr"]*10000 + SWATchannelData["mon"]*100 + SWATchannelData["day"]).apply(str),format='%Y%m%d')
    SWATchannelData['date'] = SWATchannelData['datetime'].dt.date
    SWATchannelData['time'] = SWATchannelData['datetime'].dt.time
    # in case the SWAt plus model is runing at sub daily time scale this could be utilized here:
    #if it is needed to change the timestamp
    #SWATchannelData["datetime"] = SWATchannelData["datetime"].apply(lambda x:x.replace(hour=12,minute=0))
    # cleaning up columns:
    SWATchannelData = SWATchannelData.drop(columns=['datetime','jday', 'mon', 'day', "yr"])
    # unique list of subbasins (unused currently):
    SWATSubbasinslst = SWATchannelData["unit"].unique()
    # applying filter:
    SWATchannelData = SWATchannelData[SWATchannelData["unit"].isin(SWATSubbasinsSelected)]
    # calculating the volume of water from cubic meters per second:
    SWATchannelData["vol"] = round(SWATchannelData["flo_out"] * 60 * 60 * 24,2)
    #summing volumes of water and nutrients on dates across selected subbasins
    SWATchannelDataSum = SWATchannelData.groupby(['date', 'time']).sum().reset_index()
    return SWATchannelDataSum #m3/day
#for each inflow do one of the following:

#Method 1:------------creating the concentrations rating curve for QWET-----------------------------#

# To be designed


#Method 2:------------creating the concentrations from SWAT for QWET-----------------------------#
def convert2wet(SWATchannelDataSum):
    df = pd.DataFrame()
    df["sPPOMW_konc_mg_l"] = round((SWATchannelDataSum["sedp_out"]*1000) / SWATchannelDataSum["vol"],5)
    df["sPDOMW_konc_mg_l"] = 0.0 #(sættes stadig til 0)
    df["sPO4W_konc_mg_l"]  = round((SWATchannelDataSum["solp_out"]*1000) / SWATchannelDataSum["vol"],5)
    df["sNPOMW_konc_mg_l"] = round((SWATchannelDataSum["orgn_out"]*1000) / SWATchannelDataSum["vol"],5) 
    df["sNDOMW_konc_mg_l"] = 0.0 #(sættes stadig til 0)
    df["sNO3W_konc_mg_l"]  = round(((SWATchannelDataSum["no3_out"] + SWATchannelDataSum["no2_out"])*1000) / SWATchannelDataSum["vol"],5)
    df["sNH4W_konc_mg_l"]  = round((SWATchannelDataSum["nh3_out"]*1000) / SWATchannelDataSum["vol"],5) 

    df["inflow_m3sec"]    = SWATchannelDataSum["flo_out"]

    # finally aggregate all inflows into one 

    # to be disigned

    # cleaning up nan and inf in the dataframe:
    df.replace([np.inf, -np.inf, np.nan], 0, inplace=True)
    return df
#--------------------------Export of nutrients to QWET file-------------------#
def write_wet(SWATchannelDataForExport,filePathOut):
    colsForNutrients = ['date', 'time', 'sPPOMW_konc_mg_l', 'sPDOMW_konc_mg_l', 'sPO4W_konc_mg_l', 'sNPOMW_konc_mg_l', 'sNDOMW_konc_mg_l', 'sNO3W_konc_mg_l', 'sNH4W_konc_mg_l']
    firstRow = "!" + '\t'.join(colsForNutrients)

    with open(os.path.join(filePathOut,"inflow_chem.dat"), 'w') as f:
        f.write(firstRow+'\n')

    SWATchannelDataForExport.to_csv(os.path.join(filePathOut,"inflow_chem.dat"), index = False, header=False, sep = '\t', float_format='%.7f', mode="a", columns=colsForNutrients)

    #--------------------------Export of flows to QWET file-----------------------#
    colsForInflow = ['date', 'time', 'inflow_m3sec']
    firstRow = "!" + '\t'.join(colsForInflow)

    with open(os.path.join(filePathOut,"inflow.dat"), 'w') as f:
        f.write(firstRow+'\n')

    SWATchannelDataForExport.to_csv(os.path.join(filePathOut,"inflow.dat"), index = False, header=False, sep = '\t', float_format='%.7f', mode="a", columns=colsForInflow)

def get_data_from_db(pg,vars,start,end,gis_id):
    query = f"select index,{','.join(vars)} from {pg.schema}.riv where \
                index>'{start}' and index <='{end}' and unit={gis_id}"
    res = pg.sql2dataframe(query)
    res['date'] = res['index'].dt.date
    res['time'] = res['index'].dt.time
    return res

def get_wet_data_from_swat(config,start,end,**db):

    inlets = [ x for x in config['SWATSubbasinsSelected']]
    swat_path = config['swat_path']
    wet_path = config['wet_path']
    data = []

    if db:
        for x in inlets:
            pg = PG(**db)
            dat = get_data_from_db(pg,SWATplusLookup['data'].keys(),
                            start,end,x['gis_id'])
    else:
        #swat_path = os.path.join(path,folder)
        if 'file.cio' not in os.listdir(swat_path):
            swat_path = os.path.join(swat_path,'rural')

        fil = os.path.join(swat_path,'channel_sd_day.txt')
        dat = read_swat(fil,[x['gis_id'] for x in inlets])
        dat['vol'] = dat['flo_out']*3600*24
        data.append( convert2wet(dat) )
    data = sum(data) #Er det rigtigt ?
    data['date'] = dat['date']
    data['time'] = dat['time']
    
    write_wet(data,wet_path)
    return []

