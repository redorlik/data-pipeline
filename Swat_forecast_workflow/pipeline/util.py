from datetime import datetime,timedelta
import pandas as pd
import json
import os
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError,ProgrammingError
from io import StringIO
import netCDF4 as nc
from datetime import datetime,timedelta
import numpy as np
from ..util.database import PG

# host = os.environ.get('DB_HOST',default='test.asap-forecast.com')  #'ec2-user_db_1' #
# user = os.environ.get('DB_USER',default='postgres')
# port = os.environ.get('DB_PORT',default=5432)
# passwd = os.environ.get('DB_PASSWORD',default='Swat.cat5')

run_meta = """create table \"{schema}\".model_run (
        id serial,
        forecast_date timestamp without time zone,
        model text,
        landscape text,
        last_era5_fullday timestamp without time zone
        ); """

insert_meta = """insert into \"{schema}\".model_run
    (forecast_date,model,landscape,last_era5_fullday)
    values (to_timestamp({forecast_date}),'{model}',
    '{landscape}','{last_era5_fullday}');"""
get_id = """select max(id) from \"{schema}\".model_run;"""

swatplus_output = {
         #           'stations':'stations.json',
                    'hru_pw':'hru_pw_day.txt',
                    'hru':'hru_wb_day.txt',
                    'riv':'channel_sd_day.txt',
                    'lsu':'lsunit_wb_day.txt',
                    'aquifer':'aquifer_day.txt'}

class ReadDataException(Exception):
    pass

def check_int(x):
    if type(x)!=int: 
        raise ValueError
    return int(x)

def check_timestamp(x):
    if datetime.fromtimestamp(x)<datetime.now()-timedelta(365*40):
        raise ValueError
    return float(x)
        
types = {lambda x: x.timestamp():'datetime',
        lambda x:datetime.strptime(x,'%Y-%m-%d %H:%M:%S').timestamp():'datetime',
        lambda x:datetime.strptime(x,'%Y-%m-%d').timestamp():'date',
        check_timestamp:'unix timestamp',
        check_int :'integer',
        float:'float',
        }

def guess_type(x):

    for t in types.keys():
        try:
            t(x)
        except (TypeError,ValueError,AttributeError):
            continue
        return types[t],t
    return 'string',lambda x:x


def unixtime(time):
    tp,f = guess_type(time)

    if tp in ['string']:
        print("timeobject??",time)
        res = int(time)
    else:
        res = int(f(time))
    return res

def get_stations(stations_file,stat,arg):

    print(stations_file)
    js = pd.read_json(stations_file)
    forecastDate = os.path.getmtime(stations_file)
    units = {}
    for key in js.index:
        units[key] = js[stat][key]['unit']
    
    locald = js[stat]
    param = []
    for var in arg['parameters']:
        varname = var['parameter']
        print(varname,stat)
        xtype,conv = guess_type(locald[varname]['x'][0])
        if xtype in ['datetime','date']:
            index = [unixtime(conv(x)) for x in locald[varname]['x']]
        else:
            index = locald[varname]['x']
        
        res = locald[varname]
        
        res['x'] = index
        res['unit'] = units[varname]
        res['parameter'] = varname
        res['index'] = index
        res['xaxis_type'] = guess_type(res['index'][0])[0]
        res['landscape'] = 'stations'
        res['featureId'] = stat
        res['weatherForecastProduct'] = 'NOAA Global Forecast System'
        res['forecastProduced'] = unixtime(forecastDate)
        res['nextForecast'] = '' #unixtime(forecastDate)+3600*24
        param.append(res)
    result = {}
    result['parameters'] = param
    result['weatherForecastProduct'] = 'NOAA Global Forecast System'
    result['forecastProduced'] = unixtime(forecastDate)
    result['nextForecast'] = '' #unixtime(forecastDate)+3600*24
    return result
    
def get_lookups():
    p = '.'
    with open(f'{p}/hru_lookup.json','r') as f:
        hru = json.load(f)
        for k in hru:
            hru[k]['riv'] = hru[k]['Channel']
    with open(f'{p}/lsu_lookup.json','r') as f:
        lsu = json.load(f)
        for k in lsu:
            lsu[k]['riv'] = lsu[k]['Channel']
    with open(f'{p}/riv_lookup.json','r') as f:
        riv = json.load(f)
        #for k in riv:
        #    lsu[k]['riv'] = lsu[k]['Channel']
    stations = {'station1','station2','station3','station4'}    
    return {'riv':riv,'hru':hru,'lsu':lsu,'stations':stations}
def parse_date(arg):
    if arg:
        if len(arg)==10:
            return datetime.strptime(arg,"%Y-%m-%d")
        return datetime.strptime(arg,"%Y-%m-%dT%H:%M:%S")

def get_datetime(arg):
    datetypes = {tuple:datetime,
                str: parse_date,
                int: datetime.fromtimestamp}

    return datetypes[type(arg)](arg)

def get_start_end(arg):
    start = arg['timeStart']
    start = get_datetime(start)

    end = arg['timeEnd']
    end = get_datetime(end)
    print(start,',',end)
    return start,end

def read_swat(fil,name):
    print(fil)
    if 'channel_' in fil:
        with open(fil,'r') as f:
            tmp = f.read().splitlines()
        if tmp[1].startswith('              '):    
            with open(fil,'w') as f:
                end_pos = tmp[2].find('name')+6
                tmp[1] = tmp[2][:end_pos]+tmp[1][end_pos:]
                tmp[2] = ' '*end_pos + tmp[2][end_pos+1:]
                f.write('\n'.join(tmp))
    d = pd.read_csv(fil,delimiter='\s+',
            converters={'gis_id':lambda x:str(x)},
            skiprows=[0,2])
    d['month']=d['mon']
    d['year']=d['yr']
    if name == 'lsu':
        d['gis_id']=[str(int(x[3:])) for x in d['name'].values]
    d.index = pd.to_datetime(d[['year','month','day']]) + timedelta(1/2)
    print(d.index)
    d = d.drop(columns=['month','year'])
    i = 0
    # remove null columns
    while (f'null.{i}' if i else 'null') in d.columns:
        if i:
            d = d.drop(f'null.{i}',1)
        else:
            d = d.drop('null',1)
        i += 1
    with open(fil,'r') as f:
        units = {}
        f.__next__()
        x = f.__next__()
        u = f.__next__()
        if len(u)<len(x)-5:
            u = u.split('\n')[0] + '   ---- \n'
        u = u.split()
        u.reverse()
        units['index'] = 'timestamp without time zone'

        for i,j in enumerate(u):
            idx = -1 - i
            j.replace('m^3','m<sup>3</sup>')
            units[d.columns[idx]] = j
        print(units)
    return d,units

def insert_data(data_dir,project,last_full_day=None,index=None):
    sol_tmp = pd.DataFrame()

    for name,out in swatplus_output.items():
        fil = os.path.join(data_dir,out)
        timing_start = datetime.now()
        try:
            d,units = read_swat(fil,name)
        except FileNotFoundError as e:
            print(f'File not found: {fil}')
            continue
        print('Data read into dataframe',len(d))
        if name == 'hru_pw':
            sol_tmp = d['sol_tmp']
            sol_tmp_unit = units['sol_tmp']

        if (name == 'hru') and ('sol_tmp' not in d.columns) and (not sol_tmp.empty):
            d['sol_tmp'] = sol_tmp
            units['sol_tmp'] = sol_tmp_unit
        
        forecast_date = os.path.getmtime(fil)
        #if last_full_day:
        #     q = insert_meta.format(schema=project,
        #         forecast_date=forecast_date,
        #         last_era5_fullday=last_full_day,
        #         model='swat',
        #         landscape=name)
        #     pg.execute(q)
        #     q = get_id.format(schema=project)
        #     id = pg.get_query(q)
        #     if id: id = id[0][0]
        #     print('Archive',id,q)
        # #   insert row in model_run
        # #   get id of model_run
        #     t = last_full_day
        #     mask = d.index>=datetime(t.year,t.month,t.day)
        #     d1 = d[mask].copy()
        #     d1['model_run'] = id
        #     pg.to_sql(f'archive_{name}',d1,if_exists='append')
        # # update db
        print ("Columns",d.columns)
        if 0:#index:
            df = d[d.index>=index]

            pg.dataframe2sql(name,df,replace='append')
            pg.execute(f'update "{pg.schema}"."{name}_units" set unit = \'{forecast_date}\' where var = \'forecast_date\'')
        else:
            pw = os.environ.get('DB_PASSWORD',default='rebus')
            dbport = os.environ.get('DB_PORT',default='5432')
            db=os.environ.get('DB_BASE',default='swat')
            print("DB port",dbport)
            for port in dbport.split(','):
                
                with PG(host='localhost',port=int(port),db=db,schema=project,
                            pw=pw) as pg:
                    pg_insert(pg,name,d,units,forecast_date)
        print('Insert time',datetime.now()-timing_start)

def pg_insert(pg,name,d,units,forecast_date):
    pg.dataframe2sql(name,d,replace='replace')
    #pg.execute(f'alter table "{pg.schema}"."{name}" rename index to time;')
    pg.execute(f'drop table if exists "{pg.schema}"."{name}_units";\
            create table "{pg.schema}"."{name}_units" (var text, unit text)')
    pg.execute(f'insert into "{pg.schema}"."{name}_units" (var,unit) values {str(tuple(units.items()))[1:-1]}')
    pg.execute(f'update "{pg.schema}"."{name}_units" set unit= regexp_replace(unit,\'m\^3\',\'m<sup>3</sup>\')')
    pg.execute(f'insert into "{pg.schema}"."{name}_units" (var,unit) values (\'forecast_date\',\'{forecast_date}\')')
    #pg.execute(f'update "{pg.schema}"."{name}_units" set var=\'time\' where var = \'index\'')
    print(f'finished units "{pg.schema}"."{name}_units"')


def read_data(project='kaiping',data_dir='build',
        last_full_day=None):
    print(project,data_dir)
    try:
        files = os.listdir(f'{data_dir}')
    except FileNotFoundError:
        raise ReadDataException(f"No project named {project}")
    print(files)
    # try:
    #     try:
    #         res = pg.get_query(f'select max(time) from "{pg.schema}"."lsu"')
    #     except Exception as e:
    #         # no tables 
    #         print("No hru table or no schema")
    #         #pg.execute(f'drop schema if exists "{project}" cascade;create schema "{project}";')
    #         insert_data(data_dir,project,pg)
    #     else:
    #         #index = list(res)[0][0]+timedelta(days=1)
    #         #print("Start at ",index)
    #         insert_data(data_dir,project,pg,index=None)
    # except (OperationalError,ProgrammingError):
    #     #create db
    #     print("No database - create one")
    #     raise
    insert_data(data_dir,project,
        last_full_day=last_full_day,index=None)

def table_creation(project,vars,ext=''): 
    names =', \n'.join([f'\t"{v}" double precision' for v in vars])

    sql = f'''drop table if exists "{project}".wet{ext} ; 
    create table "{project}".wet{ext} (
    time timestamp without time zone,
    {names}
    );'''
    unit_sql = f'''drop table if exists "{project}".wet{ext}_units;
    create table "{project}".wet{ext}_units (
        var varchar(50),
        unit varchar(50)
    );
    '''
    print(sql,unit_sql)
    return sql,unit_sql

def prepare_units(dat,dt,vars,folder,forecast_date,ext=''):
    unit = {'time': 'timestamp without timezone'}
    for v in vars:
        unit[v] = dat[v].units
    unit['forecast_date'] = forecast_date
    insert_units = f'insert into "{folder}".wet{ext}_units (var,unit) values \
            {", ".join([str(x) for x in unit.items()])}'
    return insert_units
    
def prepare_insert(dat,dt,vars,dims='_2d'):
    var_array = []
    for v in vars:
        var_array.append(np.array(dat[v]).reshape(dat[v].shape[0]*dat[v].shape[1]))
    print("Var_array",len(var_array),var_array[0].shape)
    var_dat = np.array(var_array)
    
    if dims=='_2d':
        tm = np.repeat(dt,dat['z'].shape[1])
        tm = tm.reshape(1,tm.shape[0])
        #vals = [str((str(tm[i]),)+tuple(var_dat[:,i])) for i in range(tm.shape[0])]
        print(tm.shape,var_dat.shape)
    else:
        tm = dt.reshape(1,dt.shape[0])
    print("Shapes",tm.shape,var_dat.shape)
    vals = np.concatenate((tm,var_dat),axis=0)
    
    print(len(vals))
    columns = ['time']+[f'"{v}"' for v in vars]
    # inserts = []
    # inserts.append(f'{", ".join(vals)}')
    fmt = '%s'+'%18e'*len(vars)
    buf = StringIO()
    #buf.write(f','.join(columns)+'\n')
    #buf.write("\n".join(inserts))
    #np.savetxt(buf,vals.T,fmt=fmt,delimiter=',')
    for i in range(var_dat.shape[1]):
        buf.write(f"'{str(tm[0,i])}'"+', ')
        for j in range(var_dat.shape[0]):
            buf.write(str(var_dat[j,i]))
            if j<len(columns)-2 :
                buf.write(', ')
        buf.write('\n')
    return buf

def create_insert(folder,vars,dat,dt,forecast_date,dims='_2d'):   
    wet_table,wet_units = table_creation(folder,vars,dims)
    pw = os.environ.get('DB_PASSWORD',default='rebus')
    dbport = os.environ.get('DB_PORT',default='5432')
    db=os.environ.get('DB_BASE',default='swat')

    for port in dbport.split(','):
        with PG(host='localhost',port=int(port),db=db,schema=folder,
                    pw=pw) as pg:
            pg.execute(wet_table)
            pg.execute(wet_units)
            buf = prepare_insert(dat,dt,vars,dims)
            pg.copy_from(buf,f'"{folder}".wet{dims}')

            insert_units = prepare_units(dat,dt,vars,folder,forecast_date,dims)
            pg.execute(insert_units)

def wet2db(data_dir,folder):
    import time
    start = time.perf_counter()
    fil = os.path.join(data_dir,'output.nc')
    forecast_date = os.path.getmtime(fil)
    dat = nc.Dataset(fil)
    units = dat.variables['time'].units
    dt = nc.num2date(dat.variables['time'][:],units,only_use_cftime_datetimes=False,only_use_python_datetimes=True)
    
    dims = dat.dimensions.values()
    vars = [v for v in dat.variables.keys() if v not in [x.name for x in dims]
                 and 'z' in dat[v].coordinates.split()]
    vars= ['z'] + vars
    vars_1 = [v for v in dat.variables.keys() if v not in [x.name for x in dims]
                 and len(dat[v].shape)==3]
    create_insert(folder,vars,dat,dt,forecast_date,dims='_2d')
    create_insert(folder,vars_1,dat,dt,forecast_date,dims='_1d')
    print("Inserted!!",time.perf_counter()-start)

