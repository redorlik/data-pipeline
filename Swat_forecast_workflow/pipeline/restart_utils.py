import os
import attr
import netCDF4 as nc4
import numpy as np
from scipy.interpolate import interp1d
from datetime import datetime,timedelta
from ..util.database import PG
from Swat_forecast_workflow.pipeline.Wet_data import (Wet,Wet_restart,
    Wet_update_restart,Wet_hotstart)
from Swat_forecast_workflow.util.assimilation.ASAP_hotstart_utils import (
    initialize_observations,
    nc_update_values_main)
from os.path import join as p_join
from ..util.sms import check_output_file
from subprocess import Popen

#import matplotlib.pyplot as plt
from Swat_forecast_workflow.util.database_setup import (frontend_test,
        local_swat,climate_anydesk,climate_local)
from shutil import copy2,move,rmtree

@attr.s
class Dates:
    start = attr.ib(default=datetime.now() - timedelta(10*365))
    end = attr.ib(default=datetime.now() + timedelta(14))
    output_start = attr.ib(default=datetime.now() - timedelta(30*3))

def wet_climate(cls,folder,pth,dates,data_pipeline=True):
    model_pg = PG(**climate_anydesk)

    data = cls(placename = folder, 
                prefix = '',
                path = pth,
                start = dates.start,
                end = dates.end,
                output_start = dates.output_start,
                pg = model_pg)
    if data_pipeline: 
        data.run_data_pipeline()
    else:
        #patch meteo_file.dat
        data.query_db_GFS()
        df2 = data.gfs
        import shutil,os,numpy as np
        trunc = os.path.join(pth,'meteo_file_trunc.dat')
        meteo = os.path.join(pth,'meteo_file.dat')
        shutil.copy(trunc,meteo)
        def ffloat(x):
            if type(x) in [np.ndarray] :
                return [f'{y:.2f}' for y in x]
            else:
                return x
        df2.to_csv(meteo,sep='\t',header=False,mode='a',index=True,float_format=ffloat)
    return data

def wet_run(folder,pth,sshs):
    uploads = []
    error = []
    wet2020 = "docker run  --rm \
            -v {path}:/data --name wet redorlik/gotm-wet2020"
    cmd = wet2020.format(path=pth)

    err = check_output_file(f"{pth}/output.nc",folder)
    if err :
        error.append(err+" "+pth)

    print("Command: >>",cmd)
    p = Popen(cmd.split()) #,cwd=pth,stdout=PIPE) #,stderr=PIPE)
    p.wait()
    #wet2db(pth,folder)
    for ssh in sshs:
        u =f"scp -C {pth}/output.nc\
            {ssh.user}@{ssh.host}:{ssh.path}/{folder}/"
        cmd = u.split()
        print(cmd)
        uploads.append(Popen(cmd))
    for u in uploads:
        u.wait()
    return error

def get_restart_period(app_config,r_path,pth):
    restart_period = app_config.get("restart_period",None)
    if restart_period and 1<=len(os.listdir(r_path))<=restart_period:
        cls = Wet_update_restart
        copy2(p_join(r_path,"restart.nc"),p_join(pth,"restart.nc"))
        restart_date = datetime.now()
    else:
        rmtree(r_path)
        os.mkdir(r_path)
        cls = Wet_restart
        #   set date to observation date
        if "today" in app_config["restart_date"]:
            restart_date = datetime.now()
        # if "last_era5_day" in app_config["restart_date"]:
        #     restart_date = data.last_full_day
    return cls

def restart_date(date,data):
    if type(date) == datetime:
        return date
    try:
        return datetime.strptime(date,"%Y-%m-%dT%H:%M:%S")
    except ValueError as e:
        pass
    if type(date) == str:    
        dates = {
            "last_era5_day" :   data.last_full_day,
            "today"         :   datetime.now(),

        }
    return dates[date]

def obs_xml(app_config,restart_date,pth):
    obs_config = app_config['assimilation']['xml']
    hotstart_date = restart_date(app_config['restart_date'][0])
    obs_config['Datetime_hotstart'] = hotstart_date
    initialize_observations(obs_config)

    nc_update_values_main(obs_config,pth)
    return obs_config
z_meas = [float(x) for x in """
            -0.75
            -2.75
            -4.75
            -6.75
            -8.75
            -10.75
            -12.75
            -14.75
            -16.75
            -18.75""".split()]
z_meas.reverse()
temp_gql = """
query test_almind_nexsens(
  $EUI:String!,$from:UnixDate!,$to:UnixDate!)
  
  {{
  tempCableReadings(EUI:$EUI, from:$from, to:$to ){{
    timestamps
    errorCode
    payloads
    errorMessage
    nSensors
    {sensors}
    lastRecord
    nextRecord
  }}
}}"""
def obs_gql(obs_config,restart_date,pth):
    import requests as r
    #obs_config = app_config['assimilation']['gql']
    #Measurements from graphQL

    # Depth
    
    #query = obs_config['query']

    nrSensor = obs_config["nr_of_sensors"]
    vars = obs_config['variables']
    restart_date = datetime(restart_date.year,restart_date.month,restart_date.day)
    vars["from"] = int((restart_date-timedelta(1./24/2)).timestamp())
    vars["to"] = int(restart_date.timestamp())
    url = obs_config['qgl_endpoint']
    i = 1
    query = []
    while (i <= nrSensor ):
        query.append( f"t{i-1}: temp(index:{i})")
        i += 1
    q = temp_gql.format(sensors = "\n".join(query) )
    res = r.post(url,json={'query':q,'variables':vars})
    
    dat = res.json()
    dat = dat['data']

    temp = [dat['tempCableReadings'][f't{i}'][0] for i in range(10)]
    temp.reverse()
    param = {"name":"temp","values":temp}
    return param,z_meas

def assimilate(pth,param):
     # data from models restart point
    p_name = param["name"]
    p_values = param["values"]
    nc = nc4.Dataset(os.path.join(pth,"restart.nc"),'r+')
    temp_nc = nc[p_name][:].flatten()
    print (temp_nc)
    h = np.array(nc["h"][..., 0, 0])
    h_cumsum = h.cumsum(axis=1)
    z = h_cumsum - h_cumsum[:, -1, np.newaxis] - h / 2
    z =z.flatten()

    # interpolation function from measured data
    
    f = interp1d(z_meas,p_values,kind='linear',fill_value="extrapolate")
    interp = f(z).flatten()
    nc[f"{p_name}"][0,:,0,0] = interp
    print(interp)
    results= {"old_vals":temp_nc,"new_vals":interp,"depths":z}
    save_to_file(z_meas,p_values,z,temp_nc,interp)
    nc.close()
    return results

def save_to_file(z_meas,meas,z,initial,interp):
    time = int(datetime.now().timestamp())
    with open(f"results{time}.log","w") as f:
        f.write("\nz:")
        f.write(str(list(z)))
        f.write("\nz_meas:")
        f.write(str(list(z_meas)))
        f.write("\nmeas:")
        f.write(str(meas))
        f.write("\ninitial:")
        f.write(str(list(initial)))
        f.write("\ninterpolated:")
        f.write(str(list(interp)))

    # plt.plot(meas,z_meas,'+')
    # plt.plot(initial,z,'v')
    # plt.plot(interp,z,'^')

if __name__=='__main__':
    import os, json
    root = "/Users/au261214/projects/Swat_app/scripts/build/almind"
    with open(os.path.join(root,"config.json"),"r") as f:
        app_config = json.load(f)
    
    obs_gql(app_config,datetime(2022,9,15),root)