from setuptools import setup
#python Swat_forecast_workflow\pipeline\Climate2Swat.py --host test.asap-forecast.com

setup(
    name = "ASAP_forecast",
    version = "0.4",
    author = "Anders Lehmann",
    author_email = "redorlik@gmail.com",
    description = ("Make forecast with SWAT"),
    license = "BSD",
    keywords = "forecast,water,soil,swat",
    url = "",
    packages=['Swat_forecast_workflow/pipeline','Swat_forecast_workflow/util'],
    long_description="",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
    install_requires=[
          'click-datetime','click','pandas','attr',
          'pytest',
      ],
     scripts=['Swat_forecast_workflow/pipeline/Climate2Swat.py']
)
